import os
import pickle
import random

import numpy as np
import pymysql.cursors
import base64


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

def setup(self):
    # Not relevant yet
    pass

def act(self, game_state: dict) -> str:
    if not game_state:
        print("No game state")
    return ACTIONS[self.model.predict(game_state)]

            
