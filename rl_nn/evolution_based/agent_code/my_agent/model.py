import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import base64
import pickle

import numpy as np

import random

import pymysql.cursors

def find_max_index(array, rand=False):
    if rand and random.randint(0, 5) == 0:
        return random.randint(0, len(array)-3)

    _, index = array.max(0)
    return index

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(11, 12)
        self.fc2 = nn.Linear(12, 6)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    def predict(self, game_state: dict):
        inputs = torch.randn(11)
        index = 0

        # Get blocks around
        x, y = game_state["self"][3]
        for i in [x-1, x, x+1]:
            for k in [y-1, y, y+1]:
                inputs[index] = 1 if game_state["field"][i][k] == -1 else 0
                index += 1

        # Get nearest coin
        nearest_coin = None
        coin_distance = None
        for coin in game_state["coins"]:
            distance = np.sqrt((x - coin[0])**2 + (y - coin[0])**2)
            if coin_distance is None or distance < coin_distance:
                coin_distance = distance
                nearest_coin = coin

        if coin is None:
            nearest_coin = (x, y)

        inputs[index] = (x - nearest_coin[0]) / len(game_state["field"])
        index += 1
        inputs[index] = (y - nearest_coin[1]) /  len(game_state["field"][0])
        index += 1
        
        return find_max_index(self.forward(inputs), True)

# Upload model
connection = pymysql.connect(
        host="*******",
        port=3307,
        user="*******",
        password="*******",
        database="*******",
        cursorclass=pymysql.cursors.DictCursor
)

# Upload model
with connection:
    with connection.cursor() as cursor:
        net = Net()
        sql = "INSERT into evolution_agents (version, generation, data) values (%s, %s, %s)"
        cursor.execute(sql, ("test", 0, base64.b64encode(pickle.dumps(net))))

    connection.commit()
