import os
import pickle
import random
from collections import namedtuple, deque
from typing import List

import events as e
# from .callbacks import state_to_features
import numpy as np

ACTIONS = np.array(['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB'])


def setup_training(self):
    self.destroyed_crates = 0


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    # Reward for breaking crate
    if 'CRATE_DESTROYED' in events:
        self.destroyed_crates += 1


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):    
    points = last_game_state["self"][1]
    if 'COIN_COLLECTED' in events:
        points += 1

    # print("Points:", last_game_state["self"][1], self.destroyed_crates)
    addLog(str(points) + "  " + str(self.destroyed_crates), "rule_100_cc_r.csv")
    self.destroyed_crates = 0

def addLog(line, filename):
    with open(filename, 'a') as file:
        file.write(line)
        file.write('\n')

