import os
import pickle
import random
from collections import namedtuple, deque
from typing import List

import events as e

import numpy as np

def setup_training(self):
    self.game_states = []
    pass

def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    if old_game_state:
        # Save game states
        self.game_states.append({
            "game_state": old_game_state,
            "action": self_action,
            "events": events
        })
        if len(self.game_states) > 5:
            self.game_states = self.game_states[1:]

        # Compute rewards. Base penalty for moves that don't lead to anything
        reward = -0.2
        max_back = 1

        # Penalty for invalid action
        if 'INVALID_ACTION' in events:
            reward = -0.3
            max_back = 1

        # Penaltiy for not moving
        if len(self.get_valid_actions(self, old_game_state)) > 0:
            if old_game_state["self"][3] == new_game_state["self"][3]:
                if self_action != 'BOMB':
                    reward += -0.1
                    max_back = 1

        # Reward for found coin
        if 'COIN_COLLECTED' in events:
            reward += 3
            max_back = 3

        # Reward for bomb influence change
        bomb_before = self.model.bomb_inputs(old_game_state)
        bomb_after = self.model.bomb_inputs(new_game_state)
        if 'BOMB_DROPPED' in events:
            if self.model.player_inputs(new_game_state)[2] == 1 or self.model.crate_inputs(new_game_state)[2] == 1:
                # print("Crate or player in reach")
                reward += 2
                max_back = 3 # -> found way to crate / player
            else:
                reward += -1  # Negative reward for placing unneccessary bomb bomb
                max_back = 1
        elif bomb_before[0] != 0:
            reward += bomb_before[0] - bomb_after[0]
            max_back = 1
            
        # Reward for killing opponent
        if 'KILLED_OPPONENT' in events:
            reward += 5
            max_back = 4

        # Reward for breaking crate
        if 'CRATE_DESTROYED' in events:
            reward += 2
            max_back = 2
            self.destroyed_crates += 1

        # Increase positiv rewards
        if reward > 0:
            reward *= 5
        
        # Train model
        if reward != 0:
            split_reward(self, reward, max_back)

def split_reward(self, reward, max_back=3):
    reward_per_state = reward / len(self.game_states)
    states = self.game_states[::-1]
    for state in states:
        self.model.train(self.model.predict(state["game_state"]), self.actions.index(state["action"]), reward)
        reward -= reward_per_state

        max_back -= 1
        if max_back == 0:
            break

def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    reward = 0
    max_back = 1
    if 'GOT_KILLED' in events:
            reward = -2.0
            max_back = 4

    if 'KILLED_OPPONENT' in events:
            reward = 5
            max_back = 5

    # Train model
    if last_action:
        split_reward(self, reward, max_back)
    
    with open(self.model_file, "wb") as file:
        pickle.dump(self.model, file, protocol=pickle.HIGHEST_PROTOCOL)
