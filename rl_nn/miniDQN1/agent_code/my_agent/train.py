import os
import pickle
import random
from collections import namedtuple, deque
from typing import List

import events as e

import numpy as np
import pymysql.cursors
import base64


def setup_training(self):
    host = os.getenv('MYSQL_HOST', "oniumy.de")
    port = int(os.getenv('MYSQL_PORT', 3307))
    user = os.getenv('MYSQL_USER', "root")
    password = os.getenv('MYSQL_PASS', "y7NRTsPGUmT2tUZ8")
    database = os.getenv('MYSQL_DB', "bomberman")
    connection = pymysql.connect(
        host=host,
        port=port,
        user=user,
        password=password,
        database=database,
        cursorclass=pymysql.cursors.DictCursor
    )

    # Find model to run
    with connection:
        with connection.cursor() as cursor:
            sql = "SELECT * FROM evolution_agents WHERE last_run is NULL;"
            cursor.execute(sql)
            result = cursor.fetchone()
            if not result:
                raise Exception("Couldn't find model to run")

            self.model = pickle.loads(base64.b64decode(result["data"]))
            self.agent = result
            print("Found model with id:", result["id"])

        # Update last run
        with connection.cursor() as cursor:
            sql = "UPDATE evolution_agents SET last_run = NOW() WHERE id = %s"
            cursor.execute(sql, (result["id"],))

        connection.commit()


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    # print("game_events_occurred")
    pass


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    host = os.getenv('MYSQL_HOST', "oniumy.de")
    port = int(os.getenv('MYSQL_PORT', 3307))
    user = os.getenv('MYSQL_USER', "root")
    password = os.getenv('MYSQL_PASS', "y7NRTsPGUmT2tUZ8")
    database = os.getenv('MYSQL_DB', "bomberman")
    connection = pymysql.connect(
        host=host,
        port=port,
        user=user,
        password=password,
        database=database,
        cursorclass=pymysql.cursors.DictCursor
    )

    # Update stats
    if not self.agent["steps"]:
        self.agent["steps"] = 0

    if not self.agent["points"]:
        self.agent["points"] = 0
        
    self.agent["steps"] += last_game_state["step"]
    self.agent["points"] += last_game_state["self"][1]
    
    with connection:
        # Update stats
        with connection.cursor() as cursor:
            sql = "UPDATE evolution_agents SET steps = %s, points = %s WHERE id = %s"
            cursor.execute(sql, (self.agent["steps"], self.agent["points"], self.agent["id"]))

        connection.commit()
