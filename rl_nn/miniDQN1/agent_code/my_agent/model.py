import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import base64
import pickle

import numpy as np

import random

import pymysql.cursors

def find_max_index(array, rand=False):
    if rand and random.randint(0, 5) == 0:
        return random.randint(0, len(array)-3)

    _, index = array.max(0)
    return index
        
        

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(11, 12)
        self.fc2 = nn.Linear(12, 6)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x

    def predict(self, game_state: dict):
        inputs = torch.randn(11)
        index = 0

        # Get blocks around
        x, y = game_state["self"][3]
        for i in [x-1, x, x+1]:
            for k in [y-1, y, y+1]:
                inputs[index] = 1 if game_state["field"][i][k] == -1 else 0
                index += 1

        # Get nearest coin
        nearest_coin = None
        coin_distance = None
        for coin in game_state["coins"]:
            distance = np.sqrt((x - coin[0])**2 + (y - coin[0])**2)
            if coin_distance is None or distance < coin_distance:
                coin_distance = distance
                nearest_coin = coin

        if coin is None:
            nearest_coin = (x, y)

        inputs[index] = (x - nearest_coin[0]) / len(game_state["field"])
        index += 1
        inputs[index] = (y - nearest_coin[1]) /  len(game_state["field"][0])
        index += 1

        # print(inputs)
        
        return find_max_index(self.forward(inputs), True)


# Upload model
connection = pymysql.connect(
        host="oniumy.de",
        port=3307,
        user="root",
        password="y7NRTsPGUmT2tUZ8",
        database="bomberman",
        cursorclass=pymysql.cursors.DictCursor
)

# Upload model
with connection:
    with connection.cursor() as cursor:
        net = Net()
        sql = "INSERT into evolution_agents (version, generation, data) values (%s, %s, %s)"
        cursor.execute(sql, ("test", 0, base64.b64encode(pickle.dumps(net))))

    connection.commit()


# Some test stuff
"""
net = Net()

# print(net)
# print(net.fc1.weight)
print(net.predict(None))

# Define optimizer and loss function
optimizer = optim.SGD(net.parameters(), lr=0.01)
criterion = nn.MSELoss()

# Dummy input
input = torch.randn(1, 3)

# Dummy target
target = torch.randn(6)  # a dummy target, for example
target = target.view(1, -1)  # make it the same shape as output

loss_fn = torch.nn.MSELoss(reduction='sum')

dump = base64.b64encode(pickle.dumps(net))
learning_rate = 0.01
loss = 1
while loss > 0.01:
    network = pickle.loads(base64.b64decode(dump))
    
    loss = loss_fn(network(input), target)

    network.zero_grad()

    loss.backward()

    with torch.no_grad():
        for param in network.parameters():
            param -= learning_rate * param.grad

    # Compute output and loss again
    output = network(input)
    # print(output)
    loss = loss_fn(output, target)

    dump = base64.b64encode(pickle.dumps(network))
    
    # print("loss:", loss, len(dump), end="\n\n")
"""
