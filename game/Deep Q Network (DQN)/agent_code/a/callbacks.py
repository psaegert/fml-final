import os
import pickle
import random

import numpy as np

from utils import state_to_features


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup(self):
    self.trainable = True
    self.replay_memory = []

def act(self, game_state: dict) -> str:
    # print("act")
    if not hasattr(self, "model"):
        # print("no model")
        return ACTIONS[4]
    elif self.model == None:
        # print("model = None")
        return ACTIONS[4]

    # print("pred")

    Qs = self.model.predict(state_to_features(game_state, self.last_position).reshape(1, 17, 17, 8))[0]

    # compute temperature based on mean game length
    # graveyards are 'hot'
    # T_add = np.clip(10 * (self.T[1] / max(abs(self.T[1] - game_state["step"]), 0.1)**2)**2, 0.1, 10)
    # T = max(self.T[0], T_add)
    
    
    if random.random() < self.T:
        return np.random.choice(ACTIONS)
    else:
        return ACTIONS[np.argmax(Qs)]

    # num = np.exp(Qs * T)
    # p =  num / np.sum(num)

    # if np.isnan(p).any():
    #     print("nan in:", Qs)
    #     return ACTIONS[np.argmax(Qs)]

    # return np.random.choice(ACTIONS, p=p)
