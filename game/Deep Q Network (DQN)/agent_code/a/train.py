from typing import List
from utils import state_to_features
import numpy as np

ACTIONS = np.array(['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB'])

def setup_training(self):
    self.trainable = True
    self.replay_memory = []
    self.last_position = (7, 7)


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    if self_action != None:
        if (new_game_state["self"][3] == self.last_position):
            events.append("MOVED_BACK")

        self.replay_memory.append((state_to_features(old_game_state, self.last_position), np.where(ACTIONS == self_action)[0][0], state_to_features(new_game_state, old_game_state["self"][3]), events))

        self.last_position = old_game_state["self"][3]


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    if last_action != None:
        self.replay_memory.append((state_to_features(last_game_state, self.last_position), np.where(ACTIONS == last_action)[0][0], None, events))