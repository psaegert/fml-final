import pickle
import os
import numpy as np
import random

def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if s_ != None:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if s_ != None:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if s_ != None:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if s_ != None:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if s_ != None:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if s_ != None:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_

def array_splits_of_size(N, array):
    return np.array_split(array, len(array) // N)

def state_to_features(game_state: dict, last_position) -> np.array:
    X = np.zeros((17, 17, 8), dtype=np.int8)

    # material
    X[game_state["field"] == -1, 0] = 1
    X[game_state["field"] == 0, 1] = 1
    X[game_state["field"] == 1, 2] = 1

    # explosion
    X[game_state["explosion_map"] > 0, 3] = 1
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = (4 - bomb[1]) / 4

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = 1

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = 1
    # X[last_position[0], last_position[1], 7] = 1

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = 1

    return X#[1:-1, 1:-1, :]

def temperature(T0, current_round, max_rounds, mean_round_length):
    return max(0.1, T0 * ((current_round - max_rounds) / max_rounds)**4), (mean_round_length if random.random() < 0.2 else 400)

def epsilon(current_round, max_rounds):
    return max(0.1, 1 - current_round/max_rounds)

class RewardModel:
    def __init__(self):
        self.actual_rewards = np.array([
            0, #"MOVED_LEFT"
            0, #"MOVED_RIGHT",
            0, #"MOVED_UP",
            0, #"MOVED_DOWN",
            0, #"WAITED",
            0, #"INVALID_ACTION",
            0, #"BOMB_DROPPED",
            0, #"BOMB_EXPLODED",
            0, #"CRATE_DESTROYED",
            0, #"COIN_FOUND",
            1, #"COIN_COLLECTED",
            5, #"KILLED_OPPONENT",
            0, #"KILLED_SELF",
            0, #"GOT_KILLED",
            0, #"OPPONENT_ELIMINATED",
            0, #"SURVIVED_ROUND"
            0, #MOVED_BACK
        ])
        self.rewards = np.array([
            -3, #MOVED_LEFT
            -3, #MOVED_RIGHT
            -3, #MOVED_UP
            -3, #MOVED_DOWN
            -9, #WAITED
            -15, #INVALID_ACTION
            -3, #BOMB_DROPPED
            0, #BOMB_EXPLODED
            40, #CRATE_DESTROYED
            0, #COIN_FOUND
            150, #COIN_COLLECTED
            200, #KILLED_OPPONENT
            0, #KILLED_SELF
            -300, #GOT_KILLED
            0, #OPPONENT_ELIMINATED
            0, #SURVIVED_ROUND
            0, #MOVED_BACK
        ]) / 10
        self.events = np.array([
            "MOVED_LEFT",
            "MOVED_RIGHT",
            "MOVED_UP",
            "MOVED_DOWN",
            "WAITED",
            "INVALID_ACTION",
            "BOMB_DROPPED",
            "BOMB_EXPLODED",
            "CRATE_DESTROYED",
            "COIN_FOUND",
            "COIN_COLLECTED",
            "KILLED_OPPONENT",
            "KILLED_SELF",
            "GOT_KILLED",
            "OPPONENT_ELIMINATED",
            "SURVIVED_ROUND",
            "MOVED_BACK"
        ])
        # self.create_data_file()
    
    # def create_data_file(self):
    #     if not os.path.isfile("reward_score_data"):
    #         with open("reward_score_data.pt", "wb") as file:
    #             reward_score_data = pickle.dump([], file)

    # def get_new_rewards(self):
    #     with open("reward_score_data.pt", "rb") as file:
    #         reward_score_data = pickle.load(file)
    #         if len(reward_score_data) == 0: return self.actual_rewards

    # def save_score(self, actual_rewards):
    #     with open("reward_score_data.pt", "rb") as file:
    #         reward_score_data = pickle.load(file)

    #     reward_score_data.append([self.rewards, actual_rewards])

    #     with open("reward_score_data.pt", "wb") as file:
    #         pickle.dump(reward_score_data, file)

    def from_events(self, events):
        reward_sum = 0
        for e in events:
            reward_sum += self.rewards[np.where(self.events == e)[0][0]]
        return reward_sum
    
    def actual(self, events):
        reward_sum = 0
        for e in events:
            reward_sum += self.actual_rewards[np.where(self.events == e)[0][0]]
        return reward_sum
