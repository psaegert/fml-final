'''create_model.py'''

import tensorflow as tf



def create_model(model_id, INPUT_SHAPE):
    if model_id == 0:
        # initialize
        model = tf.keras.models.Sequential()
        # input
        model.add(tf.keras.layers.Dense(input_shape=INPUT_SHAPE, units=INPUT_SHAPE[-1], activation='relu'))
        model.add(tf.keras.layers.Dropout(0.2))
        # hidden
        model.add(tf.keras.layers.Dense(units=512, activation='relu'))
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(units=64, activation='relu'))
        model.add(tf.keras.layers.Dropout(0.1))
        # output
        model.add(tf.keras.layers.Dense(units=6, activation='linear'))
        # compile
        model.compile(loss="mse", optimizer=tf.keras.optimizers.Adam(lr=0.001), metrics=["accuracy"])

        return model