from typing import List
import numpy as np
import os
import pickle
from create_model import create_model
import random
from tqdm import tqdm

import tensorflow as tf

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

EVENTS = [
    "MOVED_LEFT",
    "MOVED_RIGHT",
    "MOVED_UP",
    "MOVED_DOWN",
    "WAITED",
    "INVALID_ACTION",
    "BOMB_DROPPED",
    "BOMB_EXPLODED",
    "CRATE_DESTROYED",
    "COIN_FOUND",
    "COIN_COLLECTED",
    "KILLED_OPPONENT",
    "KILLED_SELF",
    "GOT_KILLED",
    "OPPONENT_ELIMINATED",
    "SURVIVED_ROUND",
]

COLS = 17
ROWS = 17
SAMPLE_SIZE = 1024 * 4

DISCOUNT = 0.9

DATA_DIRECTORY = 'Z:/fml-final/data/offline_off-policy'

LOOKUP = [
    None,
    [None, 0, 6, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72, 78, 84, None],
    [None, 90, 96, 96, 102, 102, 108, 108, 114, 114, 120, 120, 126, 126, 132, 132, None],
    [None, 138, 144, 150, 156, 162, 168, 174, 180, 186, 192, 198, 204, 210, 216, 222, None],
    [None, 228, 234, 234, 240, 240, 246, 246, 252, 252, 258, 258, 264, 264, 270, 270, None],
    [None, 276, 282, 288, 294, 300, 306, 312, 318, 324, 330, 336, 342, 348, 354, 360, None],
    [None, 366, 372, 372, 378, 378, 384, 384, 390, 390, 396, 396, 402, 402, 408, 408, None],
    [None, 414, 420, 426, 432, 438, 444, 450, 456, 462, 468, 474, 480, 486, 492, 498, None],
    [None, 504, 510, 510, 516, 516, 522, 522, 528, 528, 534, 534, 540, 540, 546, 546, None],
    [None, 552, 558, 564, 570, 576, 582, 588, 594, 600, 606, 612, 618, 624, 630, 636, None],
    [None, 642, 648, 648, 654, 654, 660, 660, 666, 666, 672, 672, 678, 678, 684, 684, None],
    [None, 690, 696, 702, 708, 714, 720, 726, 732, 738, 744, 750, 756, 762, 768, 774, None],
    [None, 780, 786, 786, 792, 792, 798, 798, 804, 804, 810, 810, 816, 816, 822, 822, None],
    [None, 828, 834, 840, 846, 852, 858, 864, 870, 876, 882, 888, 894, 900, 906, 912, None],
    [None, 918, 924, 924, 930, 930, 936, 936, 942, 942, 948, 948, 954, 954, 960, 960, None],
    [None, 966, 972, 978, 984, 990, 996, 1002, 1008, 1014, 1020, 1026, 1032, 1038, 1044, 1050, None],
    None
]

def reward_from_events(events: List[str]) -> int:
    game_rewards = [
        0, 0, 0, 0, # moved
        -0.2,       # waited
        -1,         # invalid
        0,          # bombs
        0,
        0.2,        # crate
        0.3,        # coin found
        1,          # collected
        5,          # kill
        0,
        -5,         # killed
        0, 0
    ]

    reward_sum = 0
    for event in events:
        reward_sum += game_rewards[event]

    return reward_sum

def random_transform(transition):
    transformation = np.random.randint(7)

    transformed_transition = (None, None, None, transition[3], transition[4])
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if transformation == 0:

        # switch left, right
        transformed_transition[2] = [0, 1, 3, 2, 4, 5][transition[2]]
    # mirror y -> -y
    elif transformation == 1:

        # switch up, down
        transformed_transition[2] = [1, 0, 2, 3, 4, 5][transition[2]]
    # mirror point
    elif transformation == 2:
        
        # switch up, down and left, right
        transformed_transition[2] = [1, 0, 3, 2, 4, 5][transition[2]]
    # transpose
    elif transformation == 3:
        
        # switch up, left and down, right
        transformed_transition[2] = [3, 2, 1, 0, 4, 5][transition[2]]
    # rotate left
    elif transformation == 4:
        
        # rotate left
        transformed_transition[2] = [3, 0, 1, 2, 4, 5][transition[2]]
    # rotate right
    elif transformation == 5:
        
        # rotate right
        transformed_transition[2] = [1, 2, 3, 0, 4, 5][transition[2]]
    # rotate 180
    elif transformation == 6:
        
        # switch up, down and left, right
        transformed_transition[2] = [1, 0, 3, 2, 4, 5][transition[2]]
    
    return transformed_transition

def state_to_features(game_state: dict) -> np.array:

    # encoding: [material(-1, 0, 1), bomb_counter(0, 1, 2, 3, 4), explosion_value(0, 1, 2), coin(0, 1), self(0, 1), other(0, 1)]

    if game_state == None: return None

    # flatten game_state
    game_state_flat = list()

    # add material info, explosion info and placeholders
    for x in range(1, COLS - 1):
        for y in range(1, ROWS - 1):
            if x % 2 == 1 or y % 2 == 1:
                game_state_flat.extend([game_state["field"][x, y], 0, game_state["explosion_map"][x, y], 0, 0, 0])

    game_state_flat = np.array(game_state_flat)
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: game_state_flat[ LOOKUP[bomb[0][0]][bomb[0][1]] + 1 ] = bomb[1]

    # add coin bool for every field
    for coin in game_state["coins"]: game_state_flat[ LOOKUP[coin[0]][coin[1]] + 3 ] = 1

    # add agent info (self) for every field
    game_state_flat[ LOOKUP[game_state["self"][3][0]][game_state["self"][3][1]] + 4 ] = 1

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: game_state_flat[ LOOKUP[opponent[3][0]][opponent[3][1]] + 5 ] = 1

    return game_state_flat#.reshape(1, len(game_state_flat))




# -- INIT MODELS --

model = None
try:
    model = tf.keras.models.load_model("saved_model")
except:
    model = create_model(0, (1056,))

# get filenames
filenames = os.listdir(DATA_DIRECTORY)


print(tf.__version__, tf.test.gpu_device_name())
# tf.config.set_visible_devices([], 'GPU')


# main training loop
for filename in filenames:
    # -- LOAD AND TRANSFORM DATA --

    with open(DATA_DIRECTORY + "/" + filename, "rb") as file:
        transitions = pickle.load(file)


    # -- TRAIN MODEL --

    # let model predict UPDATE_TARGET_MODEL_EVERY * BATCH_SIZE transitions
    loss_history = []

    for i in tqdm(range(len(transitions) // SAMPLE_SIZE)):

        batch = random.sample(transitions, SAMPLE_SIZE)
        current_states = None
        actions = None
        new_states = []
        non_terminal_new_state_indices = []
        rewards = None

        current_states = np.array([state_to_features(transition[0]) for transition in batch])
        actions = np.array([transition[1] for transition in batch])

        for i, transition in enumerate(batch):
            if type(transition[2]) == dict:   # non-terminal
                new_states.append(state_to_features(transition[2]))
                non_terminal_new_state_indices.append(i)
        new_states = np.array(new_states)
        non_terminal_new_state_indices = np.array(non_terminal_new_state_indices)

        rewards = np.array([reward_from_events(transition[3]) for transition in batch])

        # predict future return with target model 1s
        non_terminal_new_states_qs = model.predict(new_states, batch_size=len(new_states))
        
        # first compute the old ones 1s
        current_states_qs = model.predict(current_states, batch_size=len(current_states))


        # fill current_states_qs with reward plus future return
        for i, max_future_q in zip(non_terminal_new_state_indices, np.max(non_terminal_new_states_qs, axis=1)):
            current_states_qs[i, actions[i]] = rewards[i] + DISCOUNT * max_future_q

        # fill current_states_qs with reward
        for i in np.setdiff1d(np.arange(len(current_states)), non_terminal_new_state_indices):
            current_states_qs[i, actions[i]] = rewards[i]


        # fit minibatch on updated current_states_qs 0.7s
        loss_history.extend(model.fit(current_states, current_states_qs, batch_size=64, verbose=0, shuffle=False).history["loss"])


    # update loss history
    if os.path.isfile(f"./loss_history.pt"):
        with open(f"./loss_history.pt", "rb") as file:
            saved_loss_history = pickle.load(file)
        saved_loss_history.extend(loss_history)
    else:
        saved_loss_history = loss_history
    with open(f"./loss_history.pt", "wb") as file:
        pickle.dump(saved_loss_history, file)
    loss_history = []
    
    model.save("saved_model")