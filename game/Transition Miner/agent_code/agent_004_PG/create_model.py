'''create_model.py'''

import tensorflow as tf



def create_model(model_id, INPUT_SHAPE):
    if model_id == 0:
        # initialize
        model = tf.keras.models.Sequential()
        # input
        model.add(tf.keras.layers.Dense(input_shape=INPUT_SHAPE, units=INPUT_SHAPE[-1], activation='relu'))
        model.add(tf.keras.layers.Dropout(0.2))
        # hidden
        model.add(tf.keras.layers.Dense(units=512, activation='relu'))
        model.add(tf.keras.layers.Dropout(0.2))
        model.add(tf.keras.layers.Dense(units=64, activation='relu'))
        model.add(tf.keras.layers.Dropout(0.1))
        # output
        model.add(tf.keras.layers.Dense(units=6, activation='linear'))
        # compile
        model.compile(loss="mse", optimizer=tf.keras.optimizers.Adam(lr=0.001), metrics=["accuracy"])

    if model_id == 1:
        model = tf.keras.models.Sequential()

        model.add(Conv2D(740, (3, 3), input_shape=INPUT_SHAPE))  # (17, 17, 6)
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))

        model.add(Conv2D(740, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.2))

        model.add(Flatten())
        model.add(Dense(128))

        model.add(Dense(6, activation='softmax'))
        model.compile(loss="mse", optimizer=Adam(lr=0.001), metrics=['accuracy'])
        
        return model
    