from typing import List
import numpy as np
import os
import pickle
from create_model import create_model
import random
from tqdm import tqdm

import tensorflow as tf
import time

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

EVENTS = [
    "MOVED_LEFT",
    "MOVED_RIGHT",
    "MOVED_UP",
    "MOVED_DOWN",
    "WAITED",
    "INVALID_ACTION",
    "BOMB_DROPPED",
    "BOMB_EXPLODED",
    "CRATE_DESTROYED",
    "COIN_FOUND",
    "COIN_COLLECTED",
    "KILLED_OPPONENT",
    "KILLED_SELF",
    "GOT_KILLED",
    "OPPONENT_ELIMINATED",
    "SURVIVED_ROUND",
]

COLS = 17
ROWS = 17
SAMPLE_SIZE = 64
SAVE_MODEL_EVERY = 10

DISCOUNT = 0.9

DATA_DIRECTORY = 'Z:/fml-final/data/offline_off-policy'

def reward_from_events(events: List[str]) -> int:
    game_rewards = [
        0, 0, 0, 0, # moved
        -0.2,       # waited
        -1,         # invalid
        0,          # bombs
        0,
        0.2,        # crate
        0.3,        # coin found
        1,          # collected
        5,          # kill
        0,
        -5,         # killed
        0, 0
    ]

    reward_sum = 0
    for event in events:
        reward_sum += game_rewards[event]

    return reward_sum * 100

def random_transform(transition):
    transformation = np.random.randint(7)

    transformed_transition = (None, None, None, transition[3], transition[4])
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if transformation == 0:

        # switch left, right
        transformed_transition[2] = [0, 1, 3, 2, 4, 5][transition[2]]
    # mirror y -> -y
    elif transformation == 1:

        # switch up, down
        transformed_transition[2] = [1, 0, 2, 3, 4, 5][transition[2]]
    # mirror point
    elif transformation == 2:
        
        # switch up, down and left, right
        transformed_transition[2] = [1, 0, 3, 2, 4, 5][transition[2]]
    # transpose
    elif transformation == 3:
        
        # switch up, left and down, right
        transformed_transition[2] = [3, 2, 1, 0, 4, 5][transition[2]]
    # rotate left
    elif transformation == 4:
        
        # rotate left
        transformed_transition[2] = [3, 0, 1, 2, 4, 5][transition[2]]
    # rotate right
    elif transformation == 5:
        
        # rotate right
        transformed_transition[2] = [1, 2, 3, 0, 4, 5][transition[2]]
    # rotate 180
    elif transformation == 6:
        
        # switch up, down and left, right
        transformed_transition[2] = [1, 0, 3, 2, 4, 5][transition[2]]
    
    return transformed_transition

def state_to_features(game_state: dict) -> np.array:

    # encoding: [
    #   material(0, 1, 2),
    #   bomb_counter(0, 1, 2, 3, 4),
    #   explosion_value(0, 1, 2),
    #   coin(0, 1),
    #   self(0, 1),
    #   other(0, 1)
    # ]

    X = np.zeros((17, 17, 6))

    # add material info, explosion info and placeholders
    for x in range(COLS):
        for y in range(ROWS):
            X[x, y, 0] = game_state["field"][x, y] + 1
            X[x, y, 2] = game_state["explosion_map"][x, y]
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 1] = bomb[1]

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 3] = 1

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 4] = 1

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 5] = 1

    return X

# -- INIT MODELS --

prediction_model = None
try:
    prediction_model = tf.keras.models.load_model("saved_model")
except:
    prediction_model = create_model(1, (17, 17, 6))

# get filenames
filenames = os.listdir(DATA_DIRECTORY)

print(tf.__version__, tf.test.gpu_device_name())
# tf.config.set_visible_devices([], 'GPU')

save_model_counter = 0

# main training loop
for filename in tqdm(filenames):
    # -- LOAD DATA --
    with open(DATA_DIRECTORY + "/" + filename, "rb") as file:
        transitions = pickle.load(file)

    # -- TRAIN MODEL --
    loss_history = []
    accuracy_history = []

    for i in range(len(transitions) // SAMPLE_SIZE):

        batch = random.sample(transitions, SAMPLE_SIZE)
        current_states = None
        actions = None
        new_states = []
        non_terminal_new_state_indices = []
        rewards = None

        current_states = np.array([state_to_features(transition[0]) for transition in batch])
        actions = np.array([transition[1] for transition in batch])

        for i, transition in enumerate(batch):
            if type(transition[2]) == dict:   # non-terminal
                new_states.append(state_to_features(transition[2]))
                non_terminal_new_state_indices.append(i)
        new_states = np.array(new_states)
        non_terminal_new_state_indices = np.array(non_terminal_new_state_indices)

        rewards = np.array([reward_from_events(transition[3]) for transition in batch])

        
        # first compute the old ones
        current_states_ps = prediction_model.predict(current_states, batch_size=SAMPLE_SIZE)[actions]

        print(current_states_ps.shape)

        


        # predict future return with target model
        non_terminal_new_states_ps = target_model.predict(new_states, batch_size=len(new_states))
        


        # fill current_states_qs with reward plus future return
        for i, max_future_q in zip(non_terminal_new_state_indices, np.max(non_terminal_new_states_qs, axis=1)):
            current_states_qs[i, actions[i]] = rewards[i] + DISCOUNT * max_future_q

        # fill current_states_qs with reward
        for i in np.setdiff1d(np.arange(len(current_states)), non_terminal_new_state_indices):
            current_states_qs[i, actions[i]] = rewards[i]


        # fit minibatch on updated current_states_qs
        history = prediction_model.fit(current_states, current_states_qs, batch_size=SAMPLE_SIZE, verbose=0, shuffle=False).history

        loss_history.extend(history["loss"])
        accuracy_history.extend(history["accuracy"])

        save_model_counter += 1
        if save_model_counter > SAVE_MODEL_EVERY:
            model.save(f"saved_model_{int(time.time())}")
            
            save_model_counter = 0

    # update loss history
    if os.path.isfile(f"./loss_history.pt"):
        with open(f"./loss_history.pt", "rb") as file:
            saved_loss_history = pickle.load(file)
        saved_loss_history.extend(loss_history)
    else:
        saved_loss_history = loss_history
    with open(f"./loss_history.pt", "wb") as file:
        pickle.dump(saved_loss_history, file)
    loss_history = []

    # update acc history
    if os.path.isfile(f"./accuracy_history.pt"):
        with open(f"./accuracy_history.pt", "rb") as file:
            saved_accuracy_history = pickle.load(file)
        saved_accuracy_history.extend(accuracy_history)
    else:
        saved_accuracy_history = accuracy_history
    with open(f"./accuracy_history.pt", "wb") as file:
        pickle.dump(saved_accuracy_history, file)
    accuracy_history = []
    
model.save(f"saved_model_{int(time.time())}")