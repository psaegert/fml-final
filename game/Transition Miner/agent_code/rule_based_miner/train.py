from typing import List
import events as e
# from .callbacks import state_to_features

# custom imports
import numpy as np

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

EVENTS = [
    e.MOVED_LEFT,
    e.MOVED_RIGHT,
    e.MOVED_UP,
    e.MOVED_DOWN,
    e.WAITED,
    e.INVALID_ACTION,

    e.BOMB_DROPPED,
    e.BOMB_EXPLODED,

    e.CRATE_DESTROYED,
    e.COIN_FOUND,
    e.COIN_COLLECTED,

    e.KILLED_OPPONENT,
    e.KILLED_SELF,

    e.GOT_KILLED,
    e.OPPONENT_ELIMINATED,
    e.SURVIVED_ROUND
]


def setup_training(self):
    self.replay_memory = []


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    # self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    if self_action != None:
        self.replay_memory.append((
            state_to_features(old_game_state),
            np.where(np.array(ACTIONS) == self_action)[0][0],
            state_to_features(new_game_state),
            # reward_from_events(events),
            [np.where(np.array(EVENTS) == event)[0][0] for event in events],
            # old_game_state["step"]
        ))

def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    # self.logger.debug(f'Encountered event(s) {", ".join(map(repr, events))} in final step')
    if last_action != None:
        self.replay_memory.append((
            state_to_features(last_game_state),
            np.where(np.array(ACTIONS) == last_action)[0][0],
            None,
            # reward_from_events(events),
            [np.where(np.array(EVENTS) == event)[0][0] for event in events],
            # last_game_state["step"]
        ))

    # pass to environment
    return self.replay_memory

# def reward_from_events(events: List[str]) -> int:
#     game_rewards = {
#         e.COIN_COLLECTED: 1,
#         e.KILLED_OPPONENT: 5,
#     }
#     reward_sum = 0
#     for event in events:
#         if event in game_rewards:
#             reward_sum += game_rewards[event]
#     # self.logger.info(f"Awarded {reward_sum} for events {', '.join(events)}")
#     return reward_sum

# def state_to_features(game_state: dict) -> np.array:
#     X = np.zeros((17, 17, 8), dtype=np.int8)

#     # material
#     X[game_state["field"] == -1, 0] = True
#     X[game_state["field"] == 0, 1] = True
#     X[game_state["field"] == 1, 2] = True

#     # explosion
#     X[game_state["explosion_map"] > 0, 3] = True
    
#     # add bomb countdowns for every field
#     for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = bomb[1]

#     # add coin bool for every field
#     for coin in game_state["coins"]: X[coin[0], coin[1], 5] = True

#     # add agent info (self) for every field
#     X[game_state["self"][3][0], game_state["self"][3][1], 6] = True

#     # add agent info (opponent) for every field
#     for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = True

#     enhanced_information = [game_state["self"][2]]

#     return [X[1:-1, 1:-1, :], enhanced_information]

def state_to_features(game_state: dict) -> np.array:
    X = np.zeros((17, 17, 8), dtype=np.int8)

    # material
    X[game_state["field"] == -1, 0] = 1
    X[game_state["field"] == 0, 1] = 1
    X[game_state["field"] == 1, 2] = 1

    # explosion
    X[game_state["explosion_map"] > 0, 3] = 1
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = (4 - bomb[1]) / 4

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = 1

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = 1

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = 1

    return X
