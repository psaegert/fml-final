import sys
import threading
from argparse import ArgumentParser
from time import sleep, time

import settings as s
from environment import BombeRLeWorld, GenericWorld
from fallbacks import pygame, tqdm, LOADED_PYGAME
from replay import ReplayWorld

# Function to run the game logic in a separate thread
def game_logic(world: GenericWorld, user_inputs, args):
    last_update = time()
    while True:
        now = time()
        if args.turn_based and len(user_inputs) == 0:
            sleep(0.1)
            continue
        elif world.gui is not None and (now - last_update < args.update_interval):
            sleep(args.update_interval - (now - last_update))
            continue

        last_update = now
        if world.running:
            world.do_step(user_inputs.pop(0) if len(user_inputs) else 'WAIT')

import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPool2D, ReLU
from tensorflow.keras.optimizers import Adam
import tensorflow_probability as tfp
import numpy as np
import pickle
import os

tf.config.set_visible_devices([], 'GPU')

CREATE_CHECKPOINT_EVERY = 100

with open("../../offline_feature_extraction/expected_reward.pt", "rb") as file:
    expected_reward = pickle.load(file)

class PGModel:
    def __init__(self, path=None, lr=0.003, DISCOUNT=0.95): #lr
        self.DISCOUNT = DISCOUNT
        self.lr = lr

        try:
            self.policy = tf.keras.models.load_model(path)
            print("Loaded checkpoint")
        except:
            print("Created new model")
            self.policy = tf.keras.models.Sequential([
                Conv2D(512, (2, 2), input_shape=(15, 15, 8)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5), #dropout

                Conv2D(512, (2, 2)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5),

                Conv2D(512, (2, 2)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5),

                Flatten(),

                Dense(256),
                ReLU(),
                # Dropout(0.5),

                Dense(128),
                ReLU(),
                # Dropout(0.2),

                Dense(6, activation="softmax")
            ])

            self.policy.compile(loss="categorical_crossentropy", optimizer=Adam(lr=lr), metrics=["accuracy"])

    def update(self, feature_memory, action_memory, reward_memory):
        actions = tf.convert_to_tensor(action_memory, dtype=tf.float32)
        rewards = np.array(reward_memory, dtype=np.float32)

        died_in_step = len(reward_memory)
        rewards[-1] -= expected_reward[died_in_step]

        G = np.zeros_like(rewards)
        for t in range(len(rewards)):
            G_sum = 0
            discount = 1
            for k in range(t, len(rewards)):
                G_sum += rewards[k] * discount
                discount *= self.DISCOUNT
            G[t] = G_sum
        
        losses = []
        
        for s, a, g in zip(feature_memory, action_memory, G):
            with tf.GradientTape() as tape:

                s_tensor = tf.convert_to_tensor(s.reshape(1, 15, 15, 8).astype(np.float32), dtype=tf.float32)
                
                # prevent infinities after log and zero gradient
                p = self.policy(s_tensor, training=True) * (1 - 2*sys.float_info.epsilon) + sys.float_info.epsilon
                
                dist = tfp.distributions.Categorical(probs=p)
                log_prob = dist.log_prob(a)

                loss = - tf.squeeze(log_prob) * g

                gradient = tape.gradient(loss, self.policy.trainable_variables)
                self.policy.optimizer.apply_gradients(zip(gradient, self.policy.trainable_variables))
            
            losses.append(loss.numpy())

        return losses

def main(args):
    analysis = {"reward_history": [], "game_length": [], "loss_history": []}

    parser = ArgumentParser()

    subparsers = parser.add_subparsers(dest='command_name', required=True)

    # Run arguments
    play_parser = subparsers.add_parser("play")
    agent_group = play_parser.add_mutually_exclusive_group()
    agent_group.add_argument("--my-agent", type=str, help="Play agent of name ... against three rule_based_agents")
    agent_group.add_argument("--agents", type=str, nargs="+", default=["rule_based_agent"] * s.MAX_AGENTS, help="Explicitly set the agent names in the game")
    play_parser.add_argument("--train", default=0, type=int, choices=[0, 1, 2, 3, 4],
                             help="First … agents should be set to training mode")
    play_parser.add_argument("--continue-without-training", default=False, action="store_true")
    # play_parser.add_argument("--single-process", default=False, action="store_true")

    play_parser.add_argument("--n-rounds", type=int, default=10, help="How many rounds to play")
    play_parser.add_argument("--save-replay", default=False, action="store_true", help="Store the game as .pt for a replay")
    play_parser.add_argument("--no-gui", default=False, action="store_true", help="Deactivate the user interface and play as fast as possible.")

    # Replay arguments
    replay_parser = subparsers.add_parser("replay")
    replay_parser.add_argument("replay", help="File to load replay from")

    # Interaction
    for sub in [play_parser, replay_parser]:
        sub.add_argument("--fps", type=int, default=15, help="FPS of the GUI (does not change game)")
        sub.add_argument("--turn-based", default=False, action="store_true",
                         help="Wait for key press until next movement")
        sub.add_argument("--update-interval", type=float, default=0.1,
                         help="How often agents take steps (ignored without GUI)")

        # Video?
        sub.add_argument("--make-video", default=False, action="store_true",
                         help="Make a video from the game")

    args = parser.parse_args()
    if args.command_name == "replay":
        args.no_gui = False
        args.n_rounds = 1

    has_gui = not args.no_gui
    if has_gui:
        if not LOADED_PYGAME:
            raise ValueError("pygame could not loaded, cannot run with GUI")
        pygame.init()

    # Initialize environment and agents
    if args.command_name == "play":
        agents = []
        if args.train == 0 and not args.continue_without_training:
            args.continue_without_training = True
        if args.my_agent:
            agents.append((args.my_agent, len(agents) < args.train))
            args.agents = ["rule_based_agent"] * (s.MAX_AGENTS - 1)
        for agent_name in args.agents:
            agents.append((agent_name, len(agents) < args.train))

        if args.train:
            model = PGModel(f"./agent_code/{agents[0][0]}/MCPG")

            # distribute model across trainable agents
        world = BombeRLeWorld(args, agents, model.policy)
    elif args.command_name == "replay":
        world = ReplayWorld(args)
    else:
        raise ValueError(f"Unknown command {args.command_name}")

    # Emulate Windows process spawning behaviour under Unix (for testing)
    # mp.set_start_method('spawn')

    user_inputs = []

    # Start game logic thread
    t = threading.Thread(target=game_logic, args=(world, user_inputs, args), name="Game Logic")
    t.daemon = True
    t.start()

    # Run one or more games
    for round_index in tqdm(range(args.n_rounds)):
        if not world.running:
            world.ready_for_restart_flag.wait()
            world.ready_for_restart_flag.clear()
            # --- UPDATE AGENTS HERE ---
            world.new_round(model.policy.get_weights())

        # First render
        if has_gui:
            world.render()
            pygame.display.flip()

        round_finished = False
        last_frame = time()
        user_inputs.clear()

        # Main game loop
        while not round_finished:
            if has_gui:
                # Grab GUI events
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        if world.running:
                            world.end_round()
                        world.end()
                        return
                    elif event.type == pygame.KEYDOWN:
                        key_pressed = event.key
                        if key_pressed in (pygame.K_q, pygame.K_ESCAPE):
                            world.end_round()
                        if not world.running:
                            round_finished = True
                        # Convert keyboard input into actions
                        if s.INPUT_MAP.get(key_pressed):
                            if args.turn_based:
                                user_inputs.clear()
                            user_inputs.append(s.INPUT_MAP.get(key_pressed))

                # Render only once in a while
                if time() - last_frame >= 1 / args.fps:
                    world.render()
                    pygame.display.flip()
                    last_frame = time()
                else:
                    sleep_time = 1 / args.fps - (time() - last_frame)
                    if sleep_time > 0:
                        sleep(sleep_time)
            elif not world.running:
                round_finished = True
            else:
                # Non-gui mode, check for round end in 1ms
                sleep(0.001)


        if args.train:
            for state_path, acation_path, reward_path in zip(world.state_paths, world.action_paths, world.reward_paths):
                analysis["loss_history"].append(model.update(state_path, acation_path, reward_path))

                analysis["reward_history"].append(reward_path)
            
            world.state_paths, world.action_paths, world.reward_paths = [], [], []

            analysis["game_length"].append(world.step)

            if round_index != 0 and round_index % CREATE_CHECKPOINT_EVERY == 0:
                analysis = store(args, agents[0][0], model.policy, analysis, 0)


    if args.train:
        analysis = store(args, agents[0][0], model.policy, analysis, 0)

    world.end()

def store(args, agent_name, model, analysis, analysis_file_index):
    if args.train and model != None:
        model.save(f"./agent_code/{agent_name}/MCPG/")

    # update anylsis file
    if os.path.isfile(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt"):
        with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "rb") as file:
            saved_analysis = pickle.load(file)
            for k in saved_analysis.keys(): saved_analysis[k].extend(analysis[k])
    else:
        saved_analysis = analysis

    with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "wb") as file:
        pickle.dump(saved_analysis, file)

    return {"reward_history": [], "game_length": [], "loss_history": []}


if __name__ == '__main__':
    main(sys.argv)
