from typing import List
from .callbacks import state_to_features
import events as e
import pickle

import numpy as np
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup_training(self):
    self.state_memory = []
    self.action_memory = []
    self.reward_memory = []

    # with open("../../../../offline_training/expected_reward.pt", "rb") as file:
    #     self.expected_reward = pickle.load(file)

def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    if self_action != None:
        self.state_memory.append( state_to_features(old_game_state) )
        self.action_memory.append( np.where(np.array(ACTIONS) == self_action)[0][0] )
        self.reward_memory.append( reward_from_events(events) )#+ old_game_state["self"][1] - self.expected_reward[last_game_state["step"]] )


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    if last_action != None:
        self.state_memory.append( state_to_features(last_game_state) )
        self.action_memory.append( np.where(np.array(ACTIONS) == last_action)[0][0] )
        self.reward_memory.append( reward_from_events(events) )#+ last_game_state["self"][1] - self.expected_reward[last_game_state["step"]] )

    return self.state_memory, self.action_memory, self.reward_memory

def reward_from_events(events):
    game_rewards = {
        e.KILLED_OPPONENT: 5,
        e.COIN_COLLECTED: 1,
    }
    reward_sum = 0
    for event in events:
        if event in game_rewards:
            reward_sum += game_rewards[event]

    return reward_sum