import pickle

import tensorflow as tf

import numpy as np
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

def setup(self, policy_model):
    if policy_model != None:
        self.policy = policy_model
    else:
        if not self.train:
            self.logger.info("Loading model from saved state.")
            self.policy = tf.keras.models.load_model("MCPG")


def act(self, game_state: dict) -> str:

    p = self.policy( state_to_features(game_state).reshape(1, 15, 15, 8))[0].numpy().astype(np.float64)

    return np.random.choice(ACTIONS, p=p/np.sum(p))


def state_to_features(game_state: dict) -> np.array:
    X = np.zeros((17, 17, 8), dtype=bool)

    # material
    X[game_state["field"] == -1, 0] = True
    X[game_state["field"] == 0, 1] = True
    X[game_state["field"] == 1, 2] = True

    # explosion
    X[game_state["explosion_map"] > 0, 3] = True
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = True

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = True

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = True

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = True

    return X[1:-1, 1:-1, :]
