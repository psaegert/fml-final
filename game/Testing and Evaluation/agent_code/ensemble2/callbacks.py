import pickle

# custom imports
import numpy as np
from collections import deque
import tensorflow as tf
tf.config.set_visible_devices([], 'GPU')

from .model import Net 


ACTIONS = np.array(['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB'])

class RenameUnpickler(pickle.Unpickler):
    def find_class(self, module, name):
        if module == 'agent_code.rl_nn.model':
            module = 'agent_code.ensemble2.model'

        return super(RenameUnpickler, self).find_class(module, name)

def setup(self):
    # imitator (tf)
    self.imitator = tf.keras.models.load_model("imitator")

    # miniDQN (pytorch)
    with open("model.pt", "rb") as file:
        self.miniDQN = RenameUnpickler(file).load()

    self.last_actions = deque(maxlen=4)
    self.stuck = False

    

def act(self, game_state: dict) -> str:
    # figure out if agent is stuck in a loop
    if len(self.last_actions) == 4:
        self.stuck = (self.last_actions[0] == self.last_actions[2]) and (self.last_actions[1] == self.last_actions[3]) and (self.last_actions[0] != self.last_actions[1])

    # get Qs and ps of all models
    p_imitator = self.imitator.predict(state_to_features(game_state).reshape(1, 17, 17, 8))[0]
    if self.train: self.evaluation["p_imitator"][-1].append(p_imitator)
    proposed_action_imitator = np.argmax(p_imitator)
    if self.train: self.evaluation["proposed_action_imitator"][-1].append(proposed_action_imitator)
    if self.train: self.evaluation["confidence_imitator"][-1].append(np.max(p_imitator))

    Q_miniDQN = self.miniDQN.predict(game_state).detach().numpy()
    Q_miniDQN[[4, 5]] = Q_miniDQN[[5, 4]]
    if self.train: self.evaluation["Q_miniDQN"][-1].append(Q_miniDQN)
    proposed_action_miniDQN = np.argmax(Q_miniDQN)
    if self.train: self.evaluation["proposed_action_miniDQN"][-1].append(proposed_action_miniDQN)

    # convert Q to p by applying softmax
    p_miniDQN = softmax(Q_miniDQN)
    if self.train: self.evaluation["confidence_miniDQN"][-1].append(np.max(p_miniDQN))
    
    # filter out invalid actions
    valid_actions = get_valid_actions(game_state)

    if self.train: self.evaluation["proposed_action_imitator_was_valid"][-1].append(proposed_action_imitator in valid_actions)
    if self.train: self.evaluation["proposed_action_miniDQN_was_valid"][-1].append(proposed_action_miniDQN in valid_actions)

    # initialize valid probabilities as zeros
    valid_probs_imitator = np.zeros_like(p_imitator)
    valid_probs_miniDQN = np.zeros_like(p_miniDQN)

    # fill valid probabilities with actual values
    valid_probs_imitator[valid_actions] = p_imitator[valid_actions]
    valid_probs_miniDQN[valid_actions] = p_miniDQN[valid_actions]
    
    # calculate product and renormalize
    valid_p = valid_probs_imitator * valid_probs_miniDQN
    valid_p /= np.sum(valid_p)
    if self.train: self.evaluation["normalized_confidence_ensemble"][-1].append(np.max(valid_p))

    # get out of sticky situations
    if self.train: self.evaluation["stuck"][-1].append(self.stuck)
    if self.stuck:
        # Forcing best valid miniDQN action
        best_valid_miniDQN_action = np.argmax(valid_probs_miniDQN)
        # cannot drop bomb
        if best_valid_miniDQN_action == 5 and not game_state["self"][2]:
            best_valid_miniDQN_action == np.argsort(valid_probs_miniDQN)[::-1][1]
            print(best_valid_miniDQN_action)
        self.last_actions.append(best_valid_miniDQN_action)
        if self.train: self.evaluation["action_proposed"][-1].append(best_valid_miniDQN_action)
        return ACTIONS[best_valid_miniDQN_action]
    
    # otherwise, act optimally
    best_valid_action = np.argmax(valid_p)
    self.last_actions.append(best_valid_action)
    if self.train: self.evaluation["action_proposed"][-1].append(best_valid_action)
    return ACTIONS[best_valid_action]


def get_valid_actions(game_state):
    x, y = game_state["self"][3]
    actions = np.arange(6)

    if not game_state["self"][2]: actions = actions[actions != 5]

    top_material_is_free = game_state["field"][x, y - 1] == 0
    right_material_is_free = game_state["field"][x + 1, y] == 0
    bottom_material_is_free = game_state["field"][x, y + 1] == 0
    left_material_is_free  = game_state["field"][x - 1, y] == 0

    for enemy in game_state["others"]:
        if enemy[3] == (x, y - 1): top_material_is_free = False
        elif enemy[3] == (x + 1, y): right_material_is_free = False
        elif enemy[3] == (x, y + 1): bottom_material_is_free = False
        elif enemy[3] == (x - 1, y): left_material_is_free = False

    for a in actions:
        if (a == 0 and not top_material_is_free) or (a == 1 and not right_material_is_free) or (a == 2 and not bottom_material_is_free) or (a == 3 and not left_material_is_free):
            actions = actions[actions != a]

    return actions

def softmax(Qs):
    num = np.exp(Qs)
    return num / np.sum(num)

def state_to_features(game_state: dict) -> np.array:
    X = np.zeros((17, 17, 8), dtype=bool)

    if game_state == None: return X

    # material
    X[game_state["field"] == -1, 0] = True
    X[game_state["field"] == 0, 1] = True
    X[game_state["field"] == 1, 2] = True

    # explosion
    X[game_state["explosion_map"] > 0, 3] = True
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = True

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = True

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = True

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = True

    return X

def state_to_features_miniDQN(game_state: dict) -> np.array:
    inputs = []

    # Get blocks around
    inputs += block_inputs(game_state)

    # Coin inputs
    inputs += coin_inputs(game_state)

    # Bomb inputs
    inputs += bomb_inputs(game_state)

    # Explosion inputs
    inputs += explosion_inputs(game_state)

    # Player inputs
    inputs += player_inputs(game_state)

    # Crate inputs
    inputs += crate_inputs(game_state)

    return np.array(inputs)

def block_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Get fields around and under player
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["field"][i][k] != 0 else 0)

    # Get bomb free field in each direction
    inputs.append(0)
    for i in range(x+1, min(16, x+5)):
        if game_state["field"][i][y] == 0: # Check if field is free
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break
        

    inputs.append(0)
    for i in range(x-1, max(-1, x-5), -1):
        if game_state["field"][i][y] == 0:
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y+1, min(16, y+5)):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y-1, -1, -1):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    # print(inputs[-4:])

    return inputs

def coin_inputs(game_state: dict):
    x, y = game_state["self"][3]
    
    # Find closest coin
    nearest_coin = None
    coin_distance = None
    for coin in game_state["coins"]:
        distance = np.sqrt((x - coin[0])**2 + (y - coin[1])**2)
        if coin_distance is None or distance < coin_distance:
            coin_distance = distance
            nearest_coin = coin

    if nearest_coin is None:
        return [0, 0]

    inputs = []
    inputs.append((nearest_coin[0] - x) / (coin_distance + 0.1))
    inputs.append((nearest_coin[1] - y) / (coin_distance + 0.1))

    return inputs

def bomb_inputs(game_state: dict):
    x, y = game_state["self"][3]
    inputs = []
    
    # Find relevant bombs
    fields = [(x, y), (x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    for field in fields:
        inputs.append(has_bomb(game_state, field))

    # Check if bomb is ready to place
    inputs.append(1 if game_state["self"][2] else 0)

    return inputs

def has_bomb(game_state, field):
    value = 0
    for bomb in game_state["bombs"]:
        coords = bomb[0]
        if field[0] - coords[0] != 0 and field[1] - coords[1] != 0:
            continue

        distance = np.sqrt((field[0] - coords[0])**2 + (field[1] - coords[1])**2)
        if distance > 3:
            continue

        value += 1 - distance * 0.25

    return value

def explosion_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]
    
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["explosion_map"][i][k] != 0 else 0)

    return inputs

def player_inputs(self, game_state: dict):
    x, y = game_state["self"][3]
    inputs = []

    # Find closest player
    closest_player = None
    player_distance = None
    for player in game_state["others"]:
        coords = player[3]
        distance = np.sqrt((x - coords[0])**2 + (y - coords[1])**2)
        if player_distance is None or distance < player_distance:
            player_distance = distance
            closest_player = coords

    # Distance normed to 1 as input
    if player_distance:
        inputs.append(1 - (player_distance / np.sqrt(16**2 + 16**2)))
    else:
        inputs.append(0)

    if closest_player is None:
        inputs += [0, 0]
    else:
        inputs.append((closest_player[0] - x) / (player_distance + 0.1))
        inputs.append((closest_player[1] - y) / (player_distance + 0.1))

    # Check if player is in bomb range
    if closest_player and (closest_player[0] - x == 0 or closest_player[1] - y == 0) and player_distance < 4:
        inputs.append(1)
    else:
        inputs.append(0)
        

    return inputs

def crate_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Find closest crate
    closest_crate = None
    crate_distance = None
    for i in range(len(game_state["field"])):
        for k in range(len(game_state["field"][i])):
            if game_state["field"][i][k] == 1:
                distance = np.sqrt((x - i)**2 + (y - k)**2)

                if crate_distance is None or distance < crate_distance:
                    crate_distance = distance
                    closest_crate = (i, k)

    # print(closest_crate)
    if closest_crate is None:
        inputs += [0, 0]
    else:
        if abs(closest_crate[0] - x) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[0] - x) / (crate_distance + 0.1))

        if abs(closest_crate[1] - y) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[1] - y) / (crate_distance + 0.1))

        if closest_crate[0] - x == 1 and closest_crate[1] - y == 1:
            # print("case")
            inputs[-1], inputs[-2] = 0.5, 0.5

    # Check if player is next to crate
    fields = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    inputs.append(0)
    for field in fields:
        if game_state["field"][field[0]][field[1]] == 1:
            inputs[-1] = 1
            break

    # print(inputs)
    return inputs

def manhattan(p, q):
    return abs(p[0] - q[0]) + abs(p[1] - q[1])