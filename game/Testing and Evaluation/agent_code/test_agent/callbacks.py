import os
import pickle
import random

import numpy as np

# custom imports
import tensorflow as tf
tf.config.set_visible_devices([], 'GPU')


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup(self):
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/5x5/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/16-32-64_1024-128/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/512-512-512_256-128/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/512-512-512_256-128 enhanced/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/new/MCPG_offline_off-policy")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/pre 10 0.9 0.0001/MCPG")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/pre 10 0.9 0.0001 aux pos/MCPG")
    self.model = tf.keras.models.load_model("../../../DQN/DQN")
    # self.model = tf.keras.models.load_model("../../../miniDQN/DQN")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/DQN/DQN 10 0.9 0.001 WTT/DQN/")
    # self.model = tf.keras.models.load_model("../../../MonteCarlo-PolicyGradient/agent_code/MCPG/MCPG")
    self.model.summary()
    self.last_position = (7, 7)
    self.Q_history = []

def act(self, game_state: dict) -> str:
    Qs = self.model.predict(state_to_features(game_state, self.last_position).reshape(1, 17, 17, 8))[0]

    self.Q_history.append(Qs)

    print(Qs)
    self.last_position = game_state["self"][3]
    preferable_actions = np.argsort(Qs)[::-1]
    # no bomb available
    if not game_state["self"][2]:
        preferable_actions = preferable_actions[preferable_actions != 6]

    x, y = game_state["self"][3]
    top_material_is_free = game_state["field"][x, y - 1] == 0
    right_material_is_free = game_state["field"][x + 1, y] == 0
    bottom_material_is_free = game_state["field"][x, y + 1] == 0
    left_material_is_free  = game_state["field"][x - 1, y] == 0

    for a in preferable_actions:
        if (a == 0 and not top_material_is_free) or (a == 1 and not right_material_is_free) or (a == 2 and not bottom_material_is_free) or (a == 3 and not left_material_is_free):
            preferable_actions = preferable_actions[preferable_actions != a]
    # print(preferable_actions)
    return ACTIONS[preferable_actions[0]]

    # num = np.clip(np.exp(Qs), 0.001, 1e45)
    # p =  num / np.sum(num)

    # if np.isnan(p).any():
    #     return ACTIONS[np.argmax(Qs)]

    # return np.random.choice(ACTIONS, p=p)

def state_to_features(game_state: dict, last_position) -> np.array:
    X = np.zeros((17, 17, 8), dtype=bool)

    if game_state == None: return X#[1:-1, 1:-1, :]

    # material
    X[game_state["field"] == -1, 0] = True
    X[game_state["field"] == 0, 1] = True
    X[game_state["field"] == 1, 2] = True

    # explosion
    X[game_state["explosion_map"] > 0, 3] = True
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = True

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = True

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = True
    # X[last_position[0], last_position[1], 7] = 1

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = True

    return X#[1:-1, 1:-1, :]

