import os
import pickle
import random
from collections import namedtuple, deque
from typing import List

import events as e
from .callbacks import state_to_features
import numpy as np

ACTIONS = np.array(['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB'])


def setup_training(self):
    self.evaluation = {
        "events": [],

        "p_imitator": [],
        "Q_miniDQN": [],

        "proposed_action_imitator": [],
        "proposed_action_miniDQN": [],
        "proposed_action_imitator_was_valid": [],
        "proposed_action_miniDQN_was_valid": [],

        "confidence_imitator": [],
        "confidence_miniDQN": [],
        "normalized_confidence_ensemble": [],

        "stuck": [],

        "action_proposed": [],

        "position": [],
    }

    self.last_round_index = 0


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    if new_game_state["round"] > self.last_round_index:
        self.last_round_index = new_game_state["round"]
        for k in self.evaluation.keys():
            self.evaluation[k].append([])
    
    if old_game_state != None:
        self.evaluation["events"][-1].append(events)


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):    
    self.evaluation["events"][-1].append(events)


    if not os.path.isfile("evaluation1_pos_part2.pt"):
        with open("evaluation1_pos_part2.pt", "wb") as file:
            pickle.dump(self.evaluation, file)
    else:
        with open("evaluation1_pos_part2.pt", "rb") as file:
            saved_evaluation = pickle.load(file)

        for k in saved_evaluation.keys(): saved_evaluation[k].extend(self.evaluation[k])

        with open("evaluation1_pos_part2.pt", "wb") as file:
            pickle.dump(saved_evaluation, file)
    
    self.evaluation = {
        "events": [],

        "p_imitator": [],
        "Q_miniDQN": [],

        "proposed_action_imitator": [],
        "proposed_action_miniDQN": [],
        "proposed_action_imitator_was_valid": [],
        "proposed_action_miniDQN_was_valid": [],

        "confidence_imitator": [],
        "confidence_miniDQN": [],
        "normalized_confidence_ensemble": [],

        "stuck": [],

        "action_proposed": [],

        "position": []
    }