import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import base64
import pickle

import numpy as np

import random



class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(36, 40)
        self.fc2 = nn.Linear(40, 6)
        # self.fc3 = nn.Linear(20, 6)

        # normalizing
        self.mean = None
        self.std = None

    def forward(self, x):
        x = F.relu(self.fc1(x))
        # x = F.relu(self.fc2(x))
        
        return self.fc2(x)

    def train(self, y, action, reward, learning_rate = 0.02, discount_factor = 0.95):
        # print(y, action)
        # print("")
        # print("")
        # print("Reward:", reward)
        target = reward + discount_factor * y[action].detach().numpy()

        # Create target vector
        target_vector = y.clone().detach().numpy()
        target_vector[action] = target
        # print(target, action)
        
        # Normalize
        target_vector = (target_vector - np.mean(target_vector)) / np.std(target_vector)
        # print(target_vector)

        # Adapt nn
        loss_fn = torch.nn.MSELoss(reduction='sum')
        loss = loss_fn(y, torch.tensor(target_vector))
        # print("Loss:", loss)

        self.zero_grad()
        loss.backward()

        with torch.no_grad():
            for param in self.parameters():
                param -= learning_rate * param.grad

    def predict(self, game_state: dict):
        inputs = []

        # Get blocks around
        inputs += self.block_inputs(game_state)

        # Coin inputs
        inputs += self.coin_inputs(game_state)

        # Bomb inputs
        inputs += self.bomb_inputs(game_state)

        # Explosion inputs
        inputs += self.explosion_inputs(game_state)

        # Player inputs
        inputs += self.player_inputs(game_state)

        # Crate inputs
        inputs += self.crate_inputs(game_state)

        # print("Inputs:", len(inputs))

        return self.forward(torch.tensor(inputs).to(torch.float32))

    def block_inputs(self, game_state: dict):
        inputs = []
        x, y = game_state["self"][3]

        # Get fields around and under player
        for i in [x-1, x, x+1]:
            for k in [y-1, y, y+1]:
                inputs.append(1 if game_state["field"][i][k] != 0 else 0)

        # Get bomb free field in each direction
        inputs.append(0)
        for i in range(x+1, min(16, x+5)):
            if game_state["field"][i][y] == 0: # Check if field is free
                if self.has_bomb(game_state, (i,y)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][i][y+1] == 0 and self.has_bomb(game_state, (i,y+1)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][i][y-1] == 0 and self.has_bomb(game_state, (i,y-1)) == 0:
                    inputs[-1] = 1
                    break
            else:
                break
            

        inputs.append(0)
        for i in range(x-1, max(-1, x-5), -1):
            if game_state["field"][i][y] == 0:
                if self.has_bomb(game_state, (i,y)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][i][y+1] == 0 and self.has_bomb(game_state, (i,y+1)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][i][y-1] == 0 and self.has_bomb(game_state, (i,y-1)) == 0:
                    inputs[-1] = 1
                    break
            else:
                break

        inputs.append(0)
        for i in range(y+1, min(16, y+5)):
            if game_state["field"][x][i] == 0:
                if self.has_bomb(game_state, (x,i)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][x+1][i] == 0 and self.has_bomb(game_state, (x+1,i)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][x-1][i] == 0 and self.has_bomb(game_state, (x-1,i)) == 0:
                    inputs[-1] = 1
                    break
            else:
                break

        inputs.append(0)
        for i in range(y-1, -1, -1):
            if game_state["field"][x][i] == 0:
                if self.has_bomb(game_state, (x,i)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][x+1][i] == 0 and self.has_bomb(game_state, (x+1,i)) == 0:
                    inputs[-1] = 1
                    break
                if game_state["field"][x-1][i] == 0 and self.has_bomb(game_state, (x-1,i)) == 0:
                    inputs[-1] = 1
                    break
            else:
                break

        # print(inputs[-4:])

        return inputs

    def coin_inputs(self, game_state: dict):
        x, y = game_state["self"][3]
        
        # Find closest coin
        nearest_coin = None
        coin_distance = None
        for coin in game_state["coins"]:
            distance = np.sqrt((x - coin[0])**2 + (y - coin[1])**2)
            if coin_distance is None or distance < coin_distance:
                coin_distance = distance
                nearest_coin = coin

        if nearest_coin is None:
            return [0, 0]

        inputs = []
        inputs.append((nearest_coin[0] - x) / (coin_distance + 0.1))
        inputs.append((nearest_coin[1] - y) / (coin_distance + 0.1))

        return inputs

    def bomb_inputs(self, game_state: dict):
        x, y = game_state["self"][3]
        inputs = []
        
        # Find relevant bombs
        fields = [(x, y), (x-1, y), (x+1, y), (x, y-1), (x, y+1)]
        for field in fields:
            inputs.append(self.has_bomb(game_state, field))

        # Check if bomb is ready to place
        inputs.append(1 if game_state["self"][2] else 0)

        return inputs

    def has_bomb(self, game_state, field):
        value = 0
        for bomb in game_state["bombs"]:
            coords = bomb[0]
            if field[0] - coords[0] != 0 and field[1] - coords[1] != 0:
                continue

            distance = np.sqrt((field[0] - coords[0])**2 + (field[1] - coords[1])**2)
            if distance > 3:
                continue

            value += 1 - distance * 0.25

        return value

    def explosion_inputs(self, game_state: dict):
        inputs = []
        x, y = game_state["self"][3]
        
        for i in [x-1, x, x+1]:
            for k in [y-1, y, y+1]:
                inputs.append(1 if game_state["explosion_map"][i][k] != 0 else 0)

        return inputs

    def player_inputs(self, game_state: dict):
        x, y = game_state["self"][3]
        inputs = []

        # Find closest player
        closest_player = None
        player_distance = None
        for player in game_state["others"]:
            coords = player[3]
            distance = np.sqrt((x - coords[0])**2 + (y - coords[1])**2)
            if player_distance is None or distance < player_distance:
                player_distance = distance
                closest_player = coords

        if closest_player is None:
            inputs += [0, 0]
        else:
            inputs.append((closest_player[0] - x) / (player_distance + 0.1))
            inputs.append((closest_player[1] - y) / (player_distance + 0.1))

        # Check if player is in bomb range
        if closest_player and (closest_player[0] - x == 0 or closest_player[1] - y == 0) and player_distance < 4:
            inputs.append(1)
        else:
            inputs.append(0)
            

        return inputs

    def crate_inputs(self, game_state: dict):
        inputs = []
        x, y = game_state["self"][3]

        # Find closest crate
        closest_crate = None
        crate_distance = None
        for i in range(len(game_state["field"])):
            for k in range(len(game_state["field"][i])):
                if game_state["field"][i][k] == 1:
                    distance = np.sqrt((x - i)**2 + (y - k)**2)

                    if crate_distance is None or distance < crate_distance:
                        crate_distance = distance
                        closest_crate = (i, k)

        # print(closest_crate)
        if closest_crate is None:
            inputs += [0, 0]
        else:
            if abs(closest_crate[0] - x) <= 1:
                inputs.append(0)
            else:
                inputs.append((closest_crate[0] - x) / (crate_distance + 0.1))

            if abs(closest_crate[1] - y) <= 1:
                inputs.append(0)
            else:
                inputs.append((closest_crate[1] - y) / (crate_distance + 0.1))

            if closest_crate[0] - x == 1 and closest_crate[1] - y == 1:
                # print("case")
                inputs[-1], inputs[-2] = 0.5, 0.5

        # Check if player is next to crate
        fields = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
        inputs.append(0)
        for field in fields:
            if game_state["field"][field[0]][field[1]] == 1:
                inputs[-1] = 1
                break

        # print(inputs)
        return inputs
