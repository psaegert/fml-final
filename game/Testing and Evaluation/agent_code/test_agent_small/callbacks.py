import os
import pickle
import random

import numpy as np

# custom imports
import tensorflow as tf
tf.config.set_visible_devices([], 'GPU')


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup(self):
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/5x5/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/16-32-64_1024-128/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/512-512-512_256-128/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/1_imitation/512-512-512_256-128 enhanced/imitator")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/new/MCPG_offline_off-policy")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/pre 10 0.9 0.0001/MCPG")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/2_offline_off-policy_training/pre 10 0.9 0.0001 aux pos/MCPG")
    # self.model = tf.keras.models.load_model("../../../DQN/DQN")
    # self.model = tf.keras.models.load_model("../../../miniDQN/DQN")
    self.model = tf.keras.models.load_model("../../../miniDQN/archive/DQN copy")
    # self.model = tf.keras.models.load_model("../../../../offline_feature_extraction/DQN/DQN 10 0.9 0.001 WTT/DQN/")
    # self.model = tf.keras.models.load_model("../../../MonteCarlo-PolicyGradient/agent_code/MCPG/MCPG")
    self.model.summary()
    self.last_position = (7, 7)
    # self.Q_history = []

def act(self, game_state: dict) -> str:
    Qs = self.model.predict(state_to_features(game_state, self.last_position).reshape(1, 36))[0]
    # self.Q_history.append(Qs)

    # print(Qs)
    self.last_position = game_state["self"][3]
    preferable_actions = np.argsort(Qs)[::-1]
    # no bomb available
    if not game_state["self"][2]:
        preferable_actions = preferable_actions[preferable_actions != 6]

    x, y = game_state["self"][3]
    top_material_is_free = game_state["field"][x, y - 1] == 0
    right_material_is_free = game_state["field"][x + 1, y] == 0
    bottom_material_is_free = game_state["field"][x, y + 1] == 0
    left_material_is_free  = game_state["field"][x - 1, y] == 0

    for a in preferable_actions:
        if (a == 0 and not top_material_is_free) or (a == 1 and not right_material_is_free) or (a == 2 and not bottom_material_is_free) or (a == 3 and not left_material_is_free):
            preferable_actions = preferable_actions[preferable_actions != a]
    # print(preferable_actions)
    return ACTIONS[preferable_actions[0]]

    # num = np.clip(np.exp(Qs), 0.001, 1e45)
    # p =  num / np.sum(num)

    # if np.isnan(p).any():
    #     return ACTIONS[np.argmax(Qs)]

    # return np.random.choice(ACTIONS, p=p)

def state_to_features(game_state: dict, last_position) -> np.array:
    inputs = []

    # Get blocks around
    inputs += block_inputs(game_state)

    # Coin inputs
    inputs += coin_inputs(game_state)

    # Bomb inputs
    inputs += bomb_inputs(game_state)

    # Explosion inputs
    inputs += explosion_inputs(game_state)

    # Player inputs
    inputs += player_inputs(game_state)

    # Crate inputs
    inputs += crate_inputs(game_state)

    return np.array(inputs)

def block_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Get fields around and under player
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["field"][i][k] != 0 else 0)

    # Get bomb free field in each direction
    inputs.append(0)
    for i in range(x+1, min(16, x+5)):
        if game_state["field"][i][y] == 0: # Check if field is free
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break
        

    inputs.append(0)
    for i in range(x-1, max(-1, x-5), -1):
        if game_state["field"][i][y] == 0:
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y+1, min(16, y+5)):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y-1, -1, -1):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    # print(inputs[-4:])

    return inputs

def coin_inputs(game_state: dict):
    x, y = game_state["self"][3]
    
    # Find closest coin
    nearest_coin = None
    coin_distance = None
    for coin in game_state["coins"]:
        distance = np.sqrt((x - coin[0])**2 + (y - coin[1])**2)
        if coin_distance is None or distance < coin_distance:
            coin_distance = distance
            nearest_coin = coin

    if nearest_coin is None:
        return [0, 0]

    inputs = []
    inputs.append((nearest_coin[0] - x) / (coin_distance + 0.1))
    inputs.append((nearest_coin[1] - y) / (coin_distance + 0.1))

    return inputs

def bomb_inputs(game_state: dict):
    x, y = game_state["self"][3]
    inputs = []
    
    # Find relevant bombs
    fields = [(x, y), (x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    for field in fields:
        inputs.append(has_bomb(game_state, field))

    # Check if bomb is ready to place
    inputs.append(1 if game_state["self"][2] else 0)

    return inputs

def has_bomb(game_state, field):
    value = 0
    for bomb in game_state["bombs"]:
        coords = bomb[0]
        if field[0] - coords[0] != 0 and field[1] - coords[1] != 0:
            continue

        distance = np.sqrt((field[0] - coords[0])**2 + (field[1] - coords[1])**2)
        if distance > 3:
            continue

        value += 1 - distance * 0.25

    return value

def explosion_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]
    
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["explosion_map"][i][k] != 0 else 0)

    return inputs

def player_inputs(game_state: dict):
    x, y = game_state["self"][3]
    inputs = []

    # Find closest player
    closest_player = None
    player_distance = None
    for player in game_state["others"]:
        coords = player[3]
        distance = np.sqrt((x - coords[0])**2 + (y - coords[1])**2)
        if player_distance is None or distance < player_distance:
            player_distance = distance
            closest_player = coords

    if closest_player is None:
        inputs += [0, 0]
    else:
        inputs.append((closest_player[0] - x) / (player_distance + 0.1))
        inputs.append((closest_player[1] - y) / (player_distance + 0.1))

    # Check if player is in bomb range
    if closest_player and (closest_player[0] - x == 0 or closest_player[1] - y == 0) and player_distance < 4:
        inputs.append(1)
    else:
        inputs.append(0)
        

    return inputs

def crate_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Find closest crate
    closest_crate = None
    crate_distance = None
    for i in range(len(game_state["field"])):
        for k in range(len(game_state["field"][i])):
            if game_state["field"][i][k] == 1:
                distance = np.sqrt((x - i)**2 + (y - k)**2)

                if crate_distance is None or distance < crate_distance:
                    crate_distance = distance
                    closest_crate = (i, k)

    # print(closest_crate)
    if closest_crate is None:
        inputs += [0, 0]
    else:
        if abs(closest_crate[0] - x) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[0] - x) / (crate_distance + 0.1))

        if abs(closest_crate[1] - y) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[1] - y) / (crate_distance + 0.1))

        if closest_crate[0] - x == 1 and closest_crate[1] - y == 1:
            # print("case")
            inputs[-1], inputs[-2] = 0.5, 0.5

    # Check if player is next to crate
    fields = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    inputs.append(0)
    for field in fields:
        if game_state["field"][field[0]][field[1]] == 1:
            inputs[-1] = 1
            break

    # print(inputs)
    return inputs
