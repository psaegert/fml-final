import pickle
import random
from collections import namedtuple, deque
from typing import List
import os

import events as e
from .callbacks import state_to_features

# This is only an example!
Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))

# Hyper parameters -- DO modify
TRANSITION_HISTORY_SIZE = 3  # keep only ... last transitions
RECORD_ENEMY_TRANSITIONS = 1.0  # record enemy transitions with probability ...

# Events
PLACEHOLDER_EVENT = "PLACEHOLDER"


def setup_training(self):
    self.transitions = deque(maxlen=TRANSITION_HISTORY_SIZE)


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    pass


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    # Store the model
    if not os.path.isfile("Q_history.pt"):
        with open("Q_history.pt", "wb") as file:
            pickle.dump(self.Q_history, file)
    else:
        with open("Q_history.pt", "rb") as file:
            Q_history = pickle.load(file)
        
        Q_history.extend(self.Q_history)

        with open("Q_history.pt", "wb") as file:
            pickle.dump(Q_history, file)


