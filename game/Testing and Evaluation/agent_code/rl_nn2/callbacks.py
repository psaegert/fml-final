import pickle
import os.path
import random
import numpy as np

from .model import Net

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'BOMB', 'WAIT']

def setup(self):
    self.model_file = "model.pt"
    self.actions = ACTIONS
    self.get_valid_actions = get_valid_actions
    if not os.path.isfile(self.model_file):
        self.model = Net()
        return
    
    with open(self.model_file, "rb") as file:
        self.model = pickle.load(file)

    self.predictions = []


def act(self, game_state: dict) -> str:
    if not game_state:
        print("No game state")

    # Prediction
    prediction = self.model.predict(game_state)
    _, index = prediction.max(0)
    action = self.actions[index]
    # print(prediction.detach().numpy())

    # In some cases choose second likely action
    """
    sorted_i = np.argsort(prediction.detach().numpy())[::-1]
    ratio = prediction[sorted_i[1]] / prediction[sorted_i[0]]
    if random.uniform(0, 1) < 0.05 * ratio:
        action = self.actions[sorted_i[1]]
    """

    # Epsilon greedy
    if random.uniform(0, 1) < 0.02:
        valid_actions = get_valid_actions(self, game_state)
        action = valid_actions[random.randint(0, len(valid_actions)-1)]
        # print("random action:", action)

    # save prediction
    """
    if not hasattr(self.model, 'mean') and not self.train:
        self.predictions.append(prediction.detach().numpy())
        if len(self.predictions) > 100:
            self.model.mean = np.mean(np.array(self.predictions), axis=0)
            self.model.std = np.std(np.array(self.predictions), axis=0)
            print(self.model.mean, self.model.std)
    elif not self.train:
        print((prediction.detach().numpy() - self.model.mean) / self.model.std)
    """

    # print(action)
    return action

def get_valid_actions(self, game_state):
    valid_actions = []
    if game_state["self"][2]:
        valid_actions.append('BOMB')

    block_inputs = self.model.block_inputs(game_state)
    bomb_inputs = self.model.bomb_inputs(game_state)
    explosion_inputs = self.model.explosion_inputs(game_state)
    if block_inputs[1] == 0: # Not blocked
        if bomb_inputs[1] == 0: # No bomb influence
            if explosion_inputs[1] == 0: # No explosion
                valid_actions.append('LEFT')
    if block_inputs[3] == 0:
        if bomb_inputs[3] == 0:
            if explosion_inputs[3] == 0:
                valid_actions.append('UP')
    if block_inputs[5] == 0:
        if bomb_inputs[4] == 0:
            if explosion_inputs[5] == 0:
                valid_actions.append('DOWN')
    if block_inputs[7] == 0:
        if bomb_inputs[2] == 0:
            if explosion_inputs[7] == 0:
                valid_actions.append('RIGHT')

    if len(valid_actions) == 0:
        valid_actions.append('WAIT')

    return valid_actions
