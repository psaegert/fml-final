import os
import pickle
import random

import numpy as np
from collections import deque

# custom imports
import tensorflow as tf
tf.config.set_visible_devices([], 'GPU')


ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup(self):
    self.model = tf.keras.models.load_model("imitator")
    self.model.summary()

    self.last_actions = deque(maxlen=4)
    self.stuck = False

def act(self, game_state: dict) -> str:
    self.evaluation["position"][-1].append(game_state["self"][3])
    # figure out if agent is stuck in a loop
    if len(self.last_actions) == 4:
        self.stuck = (self.last_actions[0] == self.last_actions[2]) and (self.last_actions[1] == self.last_actions[3]) and (self.last_actions[0] != self.last_actions[1])

    ps = self.model.predict(state_to_features(game_state).reshape(1, 17, 17, 8))[0]
    self.evaluation["p_imitator"][-1].append(ps)
    proposed_action_imitator = np.argmax(ps)
    self.evaluation["proposed_action_imitator"][-1].append(proposed_action_imitator)
    self.evaluation["confidence_imitator"][-1].append(np.max(ps))

    preferable_actions = np.argsort(ps)[::-1]
    # no bomb available
    if not game_state["self"][2]:
        preferable_actions = preferable_actions[preferable_actions != 5]

    x, y = game_state["self"][3]
    top_material_is_free = game_state["field"][x, y - 1] == 0
    right_material_is_free = game_state["field"][x + 1, y] == 0
    bottom_material_is_free = game_state["field"][x, y + 1] == 0
    left_material_is_free  = game_state["field"][x - 1, y] == 0

    for a in preferable_actions:
        if (a == 0 and not top_material_is_free) or (a == 1 and not right_material_is_free) or (a == 2 and not bottom_material_is_free) or (a == 3 and not left_material_is_free):
            preferable_actions = preferable_actions[preferable_actions != a]
    
    self.evaluation["proposed_action_imitator_was_valid"][-1].append(proposed_action_imitator in preferable_actions)
    self.evaluation["stuck"][-1].append(self.stuck)
    
    self.last_actions.append(preferable_actions[0])
    self.evaluation["action_proposed"][-1].append(preferable_actions[0])
    return ACTIONS[preferable_actions[0]]


def state_to_features(game_state: dict) -> np.array:
    X = np.zeros((17, 17, 8), dtype=bool)

    if game_state == None: return X

    # material
    X[game_state["field"] == -1, 0] = True
    X[game_state["field"] == 0, 1] = True
    X[game_state["field"] == 1, 2] = True

    # explosion
    X[game_state["explosion_map"] > 0, 3] = True
    
    # add bomb countdowns for every field
    for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 4] = True

    # add coin bool for every field
    for coin in game_state["coins"]: X[coin[0], coin[1], 5] = True

    # add agent info (self) for every field
    X[game_state["self"][3][0], game_state["self"][3][1], 6] = True

    # add agent info (opponent) for every field
    for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 7] = True

    return X