import sys
import threading
from argparse import ArgumentParser
from time import sleep, time

import settings as s
from environment import BombeRLeWorld, GenericWorld
from fallbacks import pygame, tqdm, LOADED_PYGAME
from replay import ReplayWorld

# Custom imports
import os
import pickle
import random
import numpy as np
from sklearn.ensemble import RandomForestRegressor as RF

CREATE_CHECKPOINT_EVERY = 50

MAX_REPLAY_MEMORY_SIZE = 1_00
DISCOUNT = 0.9
temp = 100
TEMP_DECAY = 0.9999
MIN_TEMP = 0.1
MAX_ANALYSIS_BUFFER_SIZE = 10_000

# Function to run the game logic in a separate thread
def game_logic(world: GenericWorld, user_inputs, args):
    last_update = time()
    while True:
        now = time()
        if args.turn_based and len(user_inputs) == 0:
            sleep(0.1)
            continue
        elif world.gui is not None and (now - last_update < args.update_interval):
            sleep(args.update_interval - (now - last_update))
            continue

        last_update = now
        if world.running:
            world.do_step(user_inputs.pop(0) if len(user_inputs) else 'WAIT')

class Model:

    def __init__(self, discount, temp):
        print("initializing model")
        self.actions = np.arange(6)
        self.discount = discount
        self.temp = temp
        self.reward_history = []
        self.rf = RF(n_estimators=5, max_depth=10)
        self.trained = False
        self.q_history = []

        print(self.rf.get_params())
    
    def predict(self, game_state, force=False):
        if game_state == None: return 4

        qs = self.get_qs(self.get_features(game_state))
        self.q_history.append(qs)

        if force: return np.argmax(qs)

        denominator = np.sum(np.exp(qs / self.temp))
        if denominator < 0.01:
            return np.random.randint(6)
        numerators = np.exp(qs / self.temp)
        return np.random.choice(np.arange(6), p=numerators / denominator)

    def get_qs(self, features, only_action=None):
        if self.trained:
            if only_action != None: return self.rf.predict([np.append(features, only_action)])
            return self.rf.predict([np.append(features, a) for a in self.actions])
        else:
            if only_action != None: return 0
            return np.zeros(6)

    def get_features(self, game_state: dict) -> np.array:
        X = np.zeros((17, 17, 7), dtype=bool)

        if game_state == None: return X

        # material
        X[game_state["field"] == 0, 0] = True
        X[game_state["field"] == 1, 1] = True

        # explosion
        X[game_state["explosion_map"] > 0, 2] = True
        
        # add bomb countdowns for every field
        for bomb in game_state["bombs"]: X[bomb[0][0], bomb[0][1], 3] = True

        # add coin bool for every field
        for coin in game_state["coins"]: X[coin[0], coin[1], 4] = True

        # add agent info (self) for every field
        X[game_state["self"][3][0], game_state["self"][3][1], 5] = True

        # add agent info (opponent) for every field
        for opponent in game_state["others"]: X[opponent[3][0], opponent[3][1], 6] = True

        return X[1:-1, 1:-1, :].ravel()

    def update(self, transitions):
        # difference = [r + y max Q'] - Q
        X = []
        y = []

        for transition in transitions:
            s, a, s_, ev = transition
            r = self.reward_from_events(ev, s, s_ if type(s_) == dict else s)

            X.append(np.append(self.get_features(s), a))
            y.append(r + self.discount * np.max(self.get_qs(self.get_features(s_))))
        
        print("update")
        self.rf.fit(X, y)
        self.trained = True
        

    def reward_from_events(self, events, last_game_state, new_game_state):
        game_rewards = {
            "COIN_COLLECTED": 1,
            "KILLED_OPPONENT": 5,
            "GOT_KILLED": -10,
            # "WAITED": -0.1,
            "INVALID_ACTION": -0.2,
            "CRATE_DESTROYED": 0.2,
            "COIN_FOUND": 0.2,
            "BOMB_DROPPED": 0.2
        }
        reward_sum = 0
        for event in events:
            if event in game_rewards:
                reward_sum += game_rewards[event]
        
        # punish going backwards
        visited_cell_n_steps_ago = -1 # not visited
        for i, position in enumerate(new_game_state["path"][::-1]):
            if (new_game_state["self"][3] == position).all():
                visited_cell_n_steps_ago = i + 1
                break
        if visited_cell_n_steps_ago > 0:
            reward_sum -= 0.5 / visited_cell_n_steps_ago**2

        # punish when not dropping a bomb
        # if not "BOMB_DROPPED" in events and last_game_state["self"][2]:
        #     reward_sum -= new_game_state["had_bomb_for"]

        reward_sum -= new_game_state["waited_for"]

        # print(0.1 / visited_cell_n_steps_ago, new_game_state["had_bomb_for"] )

        self.reward_history.append(reward_sum)
        return reward_sum



def main(args):

    model = None
    target_model = None
    target_model_update_counter = 0
    analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history":[], "q_history":[]}
    analysis_buffer_size = 0
    analysis_file_index = 0

    parser = ArgumentParser()

    subparsers = parser.add_subparsers(dest='command_name', required=True)

    # Run arguments
    play_parser = subparsers.add_parser("play")
    agent_group = play_parser.add_mutually_exclusive_group()
    agent_group.add_argument("--my-agent", type=str, help="Play agent of name ... against three rule_based_agents")
    agent_group.add_argument("--agents", type=str, nargs="+", default=["rule_based_agent"] * s.MAX_AGENTS, help="Explicitly set the agent names in the game")
    play_parser.add_argument("--train", default=0, type=int, choices=[0, 1, 2, 3, 4],
                             help="First … agents should be set to training mode")
    play_parser.add_argument("--continue-without-training", default=False, action="store_true")
    # play_parser.add_argument("--single-process", default=False, action="store_true")

    play_parser.add_argument("--n-rounds", type=int, default=10, help="How many rounds to play")
    play_parser.add_argument("--save-replay", default=False, action="store_true", help="Store the game as .pt for a replay")
    play_parser.add_argument("--no-gui", default=False, action="store_true", help="Deactivate the user interface and play as fast as possible.")

    # Replay arguments
    replay_parser = subparsers.add_parser("replay")
    replay_parser.add_argument("replay", help="File to load replay from")

    # Interaction
    for sub in [play_parser, replay_parser]:
        sub.add_argument("--fps", type=int, default=15, help="FPS of the GUI (does not change game)")
        sub.add_argument("--turn-based", default=False, action="store_true",
                         help="Wait for key press until next movement")
        sub.add_argument("--update-interval", type=float, default=0.1,
                         help="How often agents take steps (ignored without GUI)")

        # Video?
        sub.add_argument("--make-video", default=False, action="store_true",
                         help="Make a video from the game")

    args = parser.parse_args()
    if args.command_name == "replay":
        args.no_gui = False
        args.n_rounds = 1

    has_gui = not args.no_gui
    if has_gui:
        if not LOADED_PYGAME:
            raise ValueError("pygame could not loaded, cannot run with GUI")
        pygame.init()

    # Initialize environment and agents
    if args.command_name == "play":
        agents = []
        if args.train == 0 and not args.continue_without_training:
            args.continue_without_training = True
        if args.my_agent:
            agents.append((args.my_agent, len(agents) < args.train))
            args.agents = ["rule_based_agent"] * (s.MAX_AGENTS - 1)
        for agent_name in args.agents:
            agents.append((agent_name, len(agents) < args.train))

        if args.train:

            print("\n-------------- Attempting to load model from storage --------------")

            try:
                with open(f"./agent_code/{agents[0][0]}/model.pt", "rb") as file:
                    model = pickle.load(file)
                    model.temp = temp
                    print("Loaded model from storage")
            except:
                print("Could not load model")
                model = Model(discount=DISCOUNT, temp=temp)



            print("-------------- Finished loading model from storage --------------\n")


        world = BombeRLeWorld(args, agents, MAX_REPLAY_MEMORY_SIZE, model)
    elif args.command_name == "replay":
        world = ReplayWorld(args)
    else:
        raise ValueError(f"Unknown command {args.command_name}")

    # Emulate Windows process spawning behaviour under Unix (for testing)
    # mp.set_start_method('spawn')

    user_inputs = []

    # Start game logic thread
    t = threading.Thread(target=game_logic, args=(world, user_inputs, args), name="Game Logic")
    t.daemon = True
    t.start()

    # Run one or more games
    for round_index in tqdm(range(args.n_rounds)):
        if not world.running:
            world.ready_for_restart_flag.wait()
            world.ready_for_restart_flag.clear()
            # --- UPDATE AGENTS HERE ---
            world.new_round(model)

        # First render
        if has_gui:
            world.render()
            pygame.display.flip()

        round_finished = False
        last_frame = time()
        user_inputs.clear()

        # Main game loop
        while not round_finished:
            if has_gui:
                # Grab GUI events
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        if world.running:
                            world.end_round()
                        world.end()
                        return
                    elif event.type == pygame.KEYDOWN:
                        key_pressed = event.key
                        if key_pressed in (pygame.K_q, pygame.K_ESCAPE):
                            world.end_round()
                        if not world.running:
                            round_finished = True
                        # Convert keyboard input into actions
                        if s.INPUT_MAP.get(key_pressed):
                            if args.turn_based:
                                user_inputs.clear()
                            user_inputs.append(s.INPUT_MAP.get(key_pressed))

                # Render only once in a while
                if time() - last_frame >= 1 / args.fps:
                    world.render()
                    pygame.display.flip()
                    last_frame = time()
                else:
                    sleep_time = 1 / args.fps - (time() - last_frame)
                    if sleep_time > 0:
                        sleep(sleep_time)
            elif not world.running:
                round_finished = True
            else:
                # Non-gui mode, check for round end in 1ms
                sleep(0.001)

        if args.train:

            if len(world.joined_replay_memory) >= MAX_REPLAY_MEMORY_SIZE:
                print("training model")
                transitions = []
                for replay_memory in world.joined_replay_memory:
                    for transition in replay_memory:
                        transitions.append(transition)
                model.update(transitions)

                world.joined_replay_memory = []

            analysis_buffer_size += 1
            if analysis_buffer_size > MAX_ANALYSIS_BUFFER_SIZE:
                analysis_file_index += 1
                analysis_buffer_size = 0

            analysis["game_length"].append(world.step)
            analysis["temp"].append(model.temp)
            analysis["reward_history"].append(model.reward_history)
            model.reward_history = []
            
            analysis["q_history"].extend(model.q_history)
            model.q_history = []
                
            if model.temp > MIN_TEMP:
                model.temp *= TEMP_DECAY
                if model.temp < MIN_TEMP: model.temp = MIN_TEMP


            if round_index != 0 and round_index % CREATE_CHECKPOINT_EVERY == 0:
                store(args, agents[0][0], model, analysis, analysis_file_index)
                analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history": [], "q_history":[]}


    if args.train:
        store(args, agents[0][0], model, analysis, analysis_file_index)
        analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history": [], "q_history":[]}

    world.end()

def store(args, agent_name, model, analysis, analysis_file_index):
    if args.train and model != None:
        with open(f"./agent_code/{agent_name}/model.pt", "wb") as file:
            pickle.dump(model, file)

    # update anylsis file
    if os.path.isfile(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt"):
        with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "rb") as file:
            saved_analysis = pickle.load(file)

        saved_analysis["reward_history"].extend(analysis["reward_history"])
        saved_analysis["loss_history"].extend(analysis["loss_history"])
        saved_analysis["game_length"].extend(analysis["game_length"])
        saved_analysis["temp"].extend(analysis["temp"])
        saved_analysis["weight_history"].extend(analysis["weight_history"])
    else:
        saved_analysis = analysis


    with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "wb") as file:
        pickle.dump(saved_analysis, file)


if __name__ == '__main__':
    main(sys.argv)
