import pickle

import numpy as np
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

def setup(self, model):

    self.previous_path = []
    self.had_bomb_for = 0
    self.waited_for = 0

    if model != None:
        self.model = model
    else:
        if not self.train:
            self.logger.info("Loading model from saved state.")
            with open("model.pt", "rb") as file:
                self.model = pickle.load(file)


def act(self, game_state: dict) -> str:
    game_state["path"] = np.copy(self.previous_path)
    game_state["had_bomb_for"] = self.had_bomb_for

    self.previous_path.append(game_state["self"][3])

    action = ACTIONS[self.model.predict(game_state, not self.train)]
    
    if game_state["self"][2]:
        if action == 5:
            self.previous_path = []
            self.had_bomb_for = 0
        else:
            self.had_bomb_for += 1
    
    self.waited_for += (1 if action == 4 else 0)

    return action
