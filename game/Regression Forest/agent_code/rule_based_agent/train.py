from typing import List
# import events as e
# from .callbacks import state_to_features

# custom imports
import numpy as np
# import random

ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']



def setup_training(self):
    self.replay_memory = []


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    if self_action != None:
        self.replay_memory.append((old_game_state, np.where(np.array(ACTIONS) == self_action)[0][0], new_game_state, events))


def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    if last_action != None:
        self.replay_memory.append((last_game_state, np.where(np.array(ACTIONS) == last_action)[0][0], None, events))

    return self.replay_memory
