from typing import List

import numpy as np
ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']


def setup_training(self):
    self.previous_path = []
    self.had_bomb_for = 0
    self.waited_for = 0
    pass


def game_events_occurred(self, old_game_state: dict, self_action: str, new_game_state: dict, events: List[str]):
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in step {new_game_state["step"]}')

    if self_action != None:

        old_game_state["path"] = np.copy(self.previous_path)
        new_game_state["path"] = np.append(self.previous_path, old_game_state["self"][3])

        old_game_state["had_bomb_for"] = self.had_bomb_for
        new_game_state["had_bomb_for"] = self.had_bomb_for + (1 if self_action != "BOMB" else 0)

        old_game_state["waited_for"] = self.waited_for
        new_game_state["waited_for"] = self.waited_for + (1 if self_action == "WAITED" else 0)


        transition = (old_game_state, np.where(np.array(ACTIONS) == self_action)[0][0], new_game_state, events)

        self.model.update(transition)



def end_of_round(self, last_game_state: dict, last_action: str, events: List[str]):
    self.logger.debug(f'Encountered game event(s) {", ".join(map(repr, events))} in terminal')
    if last_action != None:
        last_game_state["path"] = np.copy(self.previous_path)
        
        last_game_state["had_bomb_for"] = self.had_bomb_for
        
        last_game_state["waited_for"] = self.waited_for

        transition = (last_game_state, np.where(np.array(ACTIONS) == last_action)[0][0], None, events)
        self.model.update(transition)

    return []