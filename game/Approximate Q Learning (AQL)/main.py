import sys
import threading
from argparse import ArgumentParser
from time import sleep, time

import settings as s
from environment import BombeRLeWorld, GenericWorld
from fallbacks import pygame, tqdm, LOADED_PYGAME
from replay import ReplayWorld

# from create_model import create_model

# Custom imports
import os
import pickle
import random
import numpy as np
import heapq

CREATE_CHECKPOINT_EVERY = 50

# UPDATE_TARGET_MODEL_EVERY = 5
MAX_REPLAY_MEMORY_SIZE = 3
# MIN_TRAIN_REPLAY_MEMORY = 1000
# SAMPLE_SIZE = 64
DISCOUNT = 0.9
# N_FUTURE = 5
N_AUGMENTATIONS = 7
# epsilon = 1
# EPSILON_DECAY = 0.9999
# MIN_EPSILON = 0.001
temp = 30
TEMP_DECAY = 0.7
MIN_TEMP = 0.1
MAX_ANALYSIS_BUFFER_SIZE = 10_000

# Function to run the game logic in a separate thread
def game_logic(world: GenericWorld, user_inputs, args):
    last_update = time()
    while True:
        now = time()
        if args.turn_based and len(user_inputs) == 0:
            sleep(0.1)
            continue
        elif world.gui is not None and (now - last_update < args.update_interval):
            sleep(args.update_interval - (now - last_update))
            continue

        last_update = now
        if world.running:
            world.do_step(user_inputs.pop(0) if len(user_inputs) else 'WAIT')

class Cell:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.f = 0
        self.g = 0
        self.free = True
        self.previous = None

class Model:

    def __init__(self, discount, temp):
        print("initializing model")
        self.actions = np.arange(6)
        self.discount = discount
        self.temp = temp
        self.difference_history = []
        self.reward_history = []
        self.weight_history = []
        self.q_history = []
        self.test_state =  {'round': 1,
            'step': 1,
            'field': np.array([[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
            [-1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1, 0, -1],
            [-1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1],
            [-1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1, 0, -1],
            [-1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1],
            [-1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, -1],
            [-1, 0, -1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1],
            [-1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, -1],
            [-1, 0, -1, 0, -1, 0, -1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1],
            [-1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, -1],
            [-1, 0, -1, 0, -1, 1, -1, 0, -1, 0, -1, 0, -1, 0, -1, 0, -1],
            [-1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, -1],
            [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]]),
            'self': ('agent_006_approx_q', 0, True, (1, 15)),
            'others': [('rule_based_agent_0', 0, True, (1, 1)), ('rule_based_agent_1', 0, True, (15, 15)), ('rule_based_agent_2', 0, True, (15, 1))],
            'bombs': [],
            'coins': [],
            'user_input': 'WAIT',
            'explosion_map': np.array([[0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.],
            [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0.]]),
            "path": np.array([(0, 0)]),
            "had_bomb_for": 0,
            "waited_for": 0
        }
        self.weights = np.zeros(len(self.get_features(self.test_state, 0)), dtype=np.float64)
    
    def predict(self, game_state, force=False):
        if game_state == None: return 4

        self.features = np.array([self.get_features(game_state, action) for action in self.actions])
        qs = self.get_qs(self.features)
        self.q_history.append(qs)

        if force: return np.argmax(qs)

        denominator = np.sum(np.exp(qs / self.temp))
        if denominator < 0.01:
            return np.random.randint(6)
        numerators = np.exp(qs / self.temp)
        return np.random.choice(np.arange(6), p=numerators / denominator)

    def get_qs(self, features, only_action=None):
        if only_action != None: return np.sum(self.weights * features[only_action])
        return np.sum(self.weights * features, axis=1)

    def astar(self, game_state, p1, p2):
        p1 = tuple(p1)
        p2 = tuple(p2)
        grid = [[Cell(x, y) for y in range(17)] for x in range(17)]

        for x, column in enumerate(game_state["field"]):
            for y, material in enumerate(column):
                if material != 0: grid[x][y].free = False
        for other in game_state["others"]:
            grid[other[3][0]][other[3][1]].free = False

        open_set = [grid[p1[0]][p1[1]]]
        closed_set = []

        while len(open_set) != 0:
            lowest_f_index = 0
            for i, cell in enumerate(open_set):
                if cell.f < open_set[lowest_f_index].f: lowest_f_index = i
            
            current = open_set[lowest_f_index]
            if current.x == p2[0] and current.y == p2[1]:
                path = [p2]
                while(current.previous != None):
                    path.append(current.previous)
                    current = grid[current.previous[0]][current.previous[1]]
                return path, len(path)

            del open_set[lowest_f_index]
            closed_set.append(current)

            neighbors = []
            for x in [current.x - 1, current.x + 1]:
                if x < 0 or x > 16: continue
                if grid[x][current.y].free: neighbors.append(grid[x][current.y])
            for y in [current.y - 1, current.y + 1]:
                if y < 0 or y > 16: continue
                if grid[current.x][y].free: neighbors.append(grid[current.x][y])

            for neighbor in neighbors:
                if not neighbor in closed_set:
                    new_g = current.g + 1
                    if neighbor in open_set:
                        if new_g < neighbor.g: neighbor.g = new_g
                    else:
                        neighbor.g = new_g
                        open_set.append(neighbor)

                    neighbor.f = neighbor.g + np.abs(neighbor.x - p2[0]) + np.abs(neighbor.y - p2[1])
                    neighbor.previous = (current.x, current.y)
        
        return [], np.inf

    def manhattan(self, p, q):
        return abs(p[0] - q[0]) + abs(p[1] - q[1])

    def distance_features(self, game_state, x, y, dx, dy, object_pos_list, use_astar=False):
        if len(object_pos_list) == 0:
            return 3, 0, 0, 0, None

        if use_astar:
            distances = [self.astar(game_state, (x,y), object_position)[1] for object_position in object_pos_list]
            min_distance_index = np.argmin(distances)
            inverse_distance_to_nearest_object = 1 / max(distances[min_distance_index], 0.5)
                
            if dx != 0 and dy != 0:
                new_distance = self.astar(game_state, (x + dx, y + dy), object_pos_list[min_distance_index])[1]
                action_delta_distance_to_closest_object = 0 if new_distance == np.inf else np.sign(new_distance - distances[min_distance_index])
            else:
                action_delta_distance_to_closest_object = 0
        
        else:
            distances = [self.manhattan(object_pos, (x, y)) for object_pos in object_pos_list]
            min_distance_index = np.argmin(distances)
            inverse_distance_to_nearest_object = 1 / max(distances[min_distance_index], 0.5)

            action_delta_distance_to_closest_object = - np.sign(dx * (object_pos_list[min_distance_index][0] - x) + dy * (object_pos_list[min_distance_index][1] - y))

        return distances[min_distance_index]/10, inverse_distance_to_nearest_object, inverse_distance_to_nearest_object**2, action_delta_distance_to_closest_object, object_pos_list[min_distance_index]

    def get_features(self, game_state, action):
        x, y = game_state["self"][3]

        # -- VICINITY --
        top_material_is_free = game_state["field"][x, y - 1] == 0
        right_material_is_free = game_state["field"][x + 1, y] == 0
        bottom_material_is_free = game_state["field"][x, y + 1] == 0
        left_material_is_free  = game_state["field"][x - 1, y] == 0

        # -- VALID MOVE --
        valid_move = (action == 0 and top_material_is_free) or (action == 1 and right_material_is_free) or (action == 2 and bottom_material_is_free) or (action == 3 and left_material_is_free)

        if valid_move:
            if action == 0:
                dx, dy = 0, -1
            elif action == 2:
                dx, dy = 0, 1
            elif action == 1:
                dx, dy = 1, 0
            elif action == 3:
                dx, dy = -1, 0
            else:
                dx, dy = 0, 0
        else:
            dx, dy = 0, 0
        
        # -- WAIT IVE BEEN HERE BEFORE -- 
        visited_cell_n_steps_ago = -1 # not visited
        for i, position in enumerate(game_state["path"][::-1]):
            if ((x + dx, y + dy) == position).all():
                visited_cell_n_steps_ago = i + 1
                break

        inv_visited_cell_n_steps_ago = 1 / visited_cell_n_steps_ago**2

        had_bomb_for = game_state["had_bomb_for"] / 10

        # -- DOES ACTION RESULT IN A BOMB BEING DROPPED -- 
        guaranteed_bomb_drop = (game_state["self"][2] and action == 5)

        # -- NUMBER OF DESTROYABLE CRATES IN VICINITY CROSS--
        number_of_destroyed_crates = 0
        if action == 5:
            for i in range(1, 4):
                if game_state["field"][x + i, y] == -1: break
                number_of_destroyed_crates += 1
            for i in range(1, 4):
                if game_state["field"][x - i, y] == -1: break
                number_of_destroyed_crates += 1
            for i in range(1, 4):
                if game_state["field"][x, y + i] == -1: break
                number_of_destroyed_crates += 1
            for i in range(1, 4):
                if game_state["field"][x, y - i] == -1: break
                number_of_destroyed_crates += 1
            
            number_of_destroyed_crates /= 9

        # -- DISTANCE TO NEAREST ENEMY --
        distance_to_nearest_enemy, inverse_distance_to_nearest_enemy, inverse_distance_to_nearest_enemy_squared, action_delta_distance_to_closest_enemy, _ = self.distance_features(game_state, x, y, dx, dy, [e[3] for e in game_state["others"]], False)

        # -- DISTANCE TO NEAREST COIN --
        distance_to_nearest_coin, inverse_distance_to_nearest_coin, inverse_distance_to_nearest_coin_squared, action_delta_distance_to_closest_coin, _ = self.distance_features(game_state, x, y, dx, dy, game_state["coins"], False)

        # -- AGENT IS NEAREST TO NEAREST COIN --
        if len(game_state["coins"]) == 0:
            nearest_to_nearest_coin = False
        else:
            nearest_to_nearest_coin = True
            nearest_coin_position = game_state["coins"][np.argmin([self.astar(game_state, (x, y), coin)[1] for coin in game_state["coins"]])]
            current_distance_to_nearest_coin = np.min([self.astar(game_state, (x, y), coin)[1] for coin in game_state["coins"]])
            for enemy_position in [e[3] for e in game_state["others"]]:
                if self.astar(game_state, enemy_position, nearest_coin_position)[1] < current_distance_to_nearest_coin:
                    nearest_to_nearest_coin = False
        
        # -- DISTANCE TO NEAREST CRATE --
        near_crate_positions = []
        for ex, column in enumerate(game_state["field"]):
            for ey, material in enumerate(column):
                if (len(game_state["field"]) > 10 and abs(x - ex) < 5 and abs(y - ey) < 5) or material == 1:
                    near_crate_positions.append((ex, ey))
        distance_to_nearest_crate, inverse_distance_to_nearest_crate, inverse_distance_to_nearest_crate_squared, action_delta_distance_to_closest_crate, _ = self.distance_features(game_state, x, y, dx, dy, near_crate_positions, False)

        next_to_crate = distance_to_nearest_crate == 1

        # -- INV_DISTANCE TO NEAREST EXPLOSION --
        explosion_positions = []
        for ex, column in enumerate(game_state["explosion_map"]):
            for ey, cooldown in enumerate(column):
                if abs(x - ex) < 4 and abs(y - ey) < 4:
                    if cooldown > 0: explosion_positions.append((ex, ey))
        distance_to_nearest_explosion, inverse_distance_to_nearest_explosion, inverse_distance_to_nearest_explosion_squared, action_delta_distance_to_closest_explosion, _ = self.distance_features(game_state, x, y, dx, dy, explosion_positions, False)

        # -- DISTANCE TO NEAREST BOMB --
        distance_to_nearest_bomb, inverse_distance_to_nearest_bomb, inverse_distance_to_nearest_bomb_squared, action_delta_distance_to_closest_bomb, nearest_bomb_position = self.distance_features(game_state, x, y, dx, dy, [b[0] for b in game_state["bombs"]], False)
        
        # -- SAME X AS NEAREST BOMB AND WITHIN RANGE --
        same_x_bomb = False if nearest_bomb_position == None else ((x == nearest_bomb_position[0]) and abs(y - nearest_bomb_position[1]) < 4)
        
        # -- SAME Y AS NEAREST BOMB AND WITHIN RANGE--
        same_y_bomb = False if nearest_bomb_position == None else ((y == nearest_bomb_position[1]) and abs(x - nearest_bomb_position[0]) < 4)

        # -- DOES ACTION PROVIDE COVER FROM NEAREST BOMB --
        action_covers_from_bomb = not same_x_bomb and not same_y_bomb
        if same_x_bomb:
            # up or down
            if action in [1, 3]:
                action_covers_from_bomb = (action == 1) or (action == 3)
            elif action == 0:
                # up is good if bomb is below (its y is bigger)
                action_covers_from_bomb = (nearest_bomb_position[1] > y)
            elif action == 2:
                action_covers_from_bomb = (nearest_bomb_position[1] < y)
        if same_y_bomb:
            # left or right
            if action in [0, 2]:
                action_covers_from_bomb = (action == 0) or (action == 2)
            elif action == 1:
                # right is good if bomb is left (its x is smaller)
                action_covers_from_bomb = (nearest_bomb_position[1] < x)
            elif action == 3:
                action_covers_from_bomb = (nearest_bomb_position[1] > x)

        return np.array([
            5,
            valid_move,
            guaranteed_bomb_drop * (number_of_destroyed_crates + had_bomb_for)**2,
            number_of_destroyed_crates,

            # distance_to_nearest_enemy,
            # inverse_distance_to_nearest_enemy,
            inverse_distance_to_nearest_enemy_squared,
            action_delta_distance_to_closest_enemy,

            # distance_to_nearest_coin,
            # inverse_distance_to_nearest_coin,
            inverse_distance_to_nearest_coin_squared,
            action_delta_distance_to_closest_coin,
            nearest_to_nearest_coin,

            # distance_to_nearest_crate,
            # inverse_distance_to_nearest_crate,
            inverse_distance_to_nearest_crate_squared,
            action_delta_distance_to_closest_crate,

            # distance_to_nearest_explosion,
            # inverse_distance_to_nearest_explosion,
            inverse_distance_to_nearest_explosion_squared,
            action_delta_distance_to_closest_explosion,

            # distance_to_nearest_bomb,
            # inverse_distance_to_nearest_bomb,
            inverse_distance_to_nearest_bomb_squared,
            action_delta_distance_to_closest_bomb,
            same_x_bomb,
            same_y_bomb,
            action_covers_from_bomb,

            inv_visited_cell_n_steps_ago,

            # had_bomb_for * distance_to_nearest_crate
        ], dtype=np.float64)

    def update(self, transition, custom_state=False, lr=0.0075):
        # difference = [r + y max Q'] - Q
        s, a, s_, ev = transition
        r = self.reward_from_events(ev, s, s_ if type(s_) == dict else s)

        current_features = self.features if not custom_state else np.array([self.get_features(s, action) for action in self.actions])

        if s_ == None:
            difference = r
            # print(r)
        else:
            new_features = np.array([self.get_features(s_, action) for action in self.actions])
            difference = r + self.discount * np.max(self.get_qs(new_features)) - self.get_qs(current_features, a)

            # print(current_features, new_features, self.get_qs(new_features), self.get_qs(current_features, a))

        correction = lr * difference * current_features[a]
        self.weights += correction

        self.difference_history.append(difference)
        self.weight_history.append(np.copy(self.weights))

    def reward_from_events(self, events, last_game_state, new_game_state):
        game_rewards = {
            "COIN_COLLECTED": 1,
            "KILLED_OPPONENT": 5,
            "GOT_KILLED": -10,
            "WAITED": -1,
            "INVALID_ACTION": -1,
            "CRATE_DESTROYED": 1,
            "COIN_FOUND": 1,
            "BOMB_DROPPED": 1
        }
        reward_sum = 0
        for event in events:
            if event in game_rewards:
                reward_sum += game_rewards[event]
        
        # punish going backwards
        visited_cell_n_steps_ago = -1 # not visited
        for i, position in enumerate(new_game_state["path"][::-1]):
            if (new_game_state["self"][3] == position).all():
                visited_cell_n_steps_ago = i + 1
                break
        if visited_cell_n_steps_ago > 0:
            reward_sum -= 0.5 / visited_cell_n_steps_ago**2

        # punish when not dropping a bomb
        if not "BOMB_DROPPED" in events and last_game_state["self"][2]:
            reward_sum -= new_game_state["had_bomb_for"]

        reward_sum -= new_game_state["waited_for"]

        # print(0.1 / visited_cell_n_steps_ago, new_game_state["had_bomb_for"] )

        self.reward_history.append(reward_sum)
        return reward_sum



def main(args):

    model = None
    target_model = None
    target_model_update_counter = 0
    analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history":[], "q_history":[]}
    analysis_buffer_size = 0
    analysis_file_index = 0

    parser = ArgumentParser()

    subparsers = parser.add_subparsers(dest='command_name', required=True)

    # Run arguments
    play_parser = subparsers.add_parser("play")
    agent_group = play_parser.add_mutually_exclusive_group()
    agent_group.add_argument("--my-agent", type=str, help="Play agent of name ... against three rule_based_agents")
    agent_group.add_argument("--agents", type=str, nargs="+", default=["rule_based_agent"] * s.MAX_AGENTS, help="Explicitly set the agent names in the game")
    play_parser.add_argument("--train", default=0, type=int, choices=[0, 1, 2, 3, 4],
                             help="First … agents should be set to training mode")
    play_parser.add_argument("--continue-without-training", default=False, action="store_true")
    # play_parser.add_argument("--single-process", default=False, action="store_true")

    play_parser.add_argument("--n-rounds", type=int, default=10, help="How many rounds to play")
    play_parser.add_argument("--save-replay", default=False, action="store_true", help="Store the game as .pt for a replay")
    play_parser.add_argument("--no-gui", default=False, action="store_true", help="Deactivate the user interface and play as fast as possible.")

    # Replay arguments
    replay_parser = subparsers.add_parser("replay")
    replay_parser.add_argument("replay", help="File to load replay from")

    # Interaction
    for sub in [play_parser, replay_parser]:
        sub.add_argument("--fps", type=int, default=15, help="FPS of the GUI (does not change game)")
        sub.add_argument("--turn-based", default=False, action="store_true",
                         help="Wait for key press until next movement")
        sub.add_argument("--update-interval", type=float, default=0.1,
                         help="How often agents take steps (ignored without GUI)")

        # Video?
        sub.add_argument("--make-video", default=False, action="store_true",
                         help="Make a video from the game")

    args = parser.parse_args()
    if args.command_name == "replay":
        args.no_gui = False
        args.n_rounds = 1

    has_gui = not args.no_gui
    if has_gui:
        if not LOADED_PYGAME:
            raise ValueError("pygame could not loaded, cannot run with GUI")
        pygame.init()

    # Initialize environment and agents
    if args.command_name == "play":
        agents = []
        if args.train == 0 and not args.continue_without_training:
            args.continue_without_training = True
        if args.my_agent:
            agents.append((args.my_agent, len(agents) < args.train))
            args.agents = ["rule_based_agent"] * (s.MAX_AGENTS - 1)
        for agent_name in args.agents:
            agents.append((agent_name, len(agents) < args.train))

        if args.train:

            print("\n-------------- Attempting to load model from storage --------------")

            try:
                with open(f"./agent_code/{agents[0][0]}/model.pt", "rb") as file:
                    model = pickle.load(file)
                    model.temp = temp
                    print("Loaded model from storage")
            except:
                print("Could not load model")
                model = Model(discount=DISCOUNT, temp=temp)



            print("-------------- Finished loading model from storage --------------\n")


        world = BombeRLeWorld(args, agents, MAX_REPLAY_MEMORY_SIZE, model)
    elif args.command_name == "replay":
        world = ReplayWorld(args)
    else:
        raise ValueError(f"Unknown command {args.command_name}")

    # Emulate Windows process spawning behaviour under Unix (for testing)
    # mp.set_start_method('spawn')

    user_inputs = []

    # Start game logic thread
    t = threading.Thread(target=game_logic, args=(world, user_inputs, args), name="Game Logic")
    t.daemon = True
    t.start()

    # Run one or more games
    for round_index in tqdm(range(args.n_rounds)):
        if not world.running:
            world.ready_for_restart_flag.wait()
            world.ready_for_restart_flag.clear()
            # --- UPDATE AGENTS HERE ---
            world.new_round(model)

        # First render
        if has_gui:
            world.render()
            pygame.display.flip()

        round_finished = False
        last_frame = time()
        user_inputs.clear()

        # Main game loop
        while not round_finished:
            if has_gui:
                # Grab GUI events
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        if world.running:
                            world.end_round()
                        world.end()
                        return
                    elif event.type == pygame.KEYDOWN:
                        key_pressed = event.key
                        if key_pressed in (pygame.K_q, pygame.K_ESCAPE):
                            world.end_round()
                        if not world.running:
                            round_finished = True
                        # Convert keyboard input into actions
                        if s.INPUT_MAP.get(key_pressed):
                            if args.turn_based:
                                user_inputs.clear()
                            user_inputs.append(s.INPUT_MAP.get(key_pressed))

                # Render only once in a while
                if time() - last_frame >= 1 / args.fps:
                    world.render()
                    pygame.display.flip()
                    last_frame = time()
                else:
                    sleep_time = 1 / args.fps - (time() - last_frame)
                    if sleep_time > 0:
                        sleep(sleep_time)
            elif not world.running:
                round_finished = True
            else:
                # Non-gui mode, check for round end in 1ms
                sleep(0.001)

        if args.train:

            # for replay_memory in world.joined_replay_memory:
            #     for transition in tqdm(replay_memory):
            #         model.update(transition, True)

            analysis_buffer_size += 1
            if analysis_buffer_size > MAX_ANALYSIS_BUFFER_SIZE:
                analysis_file_index += 1
                analysis_buffer_size = 0

            analysis["game_length"].append(world.step)
            analysis["temp"].append(model.temp)
            analysis["reward_history"].append(model.reward_history)
            model.reward_history = []
            analysis["loss_history"].append(np.mean(model.difference_history))
            model.difference_history = []
            analysis["weight_history"].extend(model.weight_history)
            model.weight_history = []
            
            analysis["q_history"].extend(model.q_history)
            model.q_history = []
                
            if model.temp > MIN_TEMP:
                model.temp *= TEMP_DECAY
                if model.temp < MIN_TEMP: model.temp = MIN_TEMP


            if round_index != 0 and round_index % CREATE_CHECKPOINT_EVERY == 0:
                store(args, agents[0][0], model, analysis, analysis_file_index)
                analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history": [], "q_history":[]}


    if args.train:
        store(args, agents[0][0], model, analysis, analysis_file_index)
        analysis = {"reward_history": [], "loss_history": [], "game_length": [], "temp": [], "weight_history": [], "q_history":[]}

    world.end()

def store(args, agent_name, model, analysis, analysis_file_index):
    if args.train and model != None:
        with open(f"./agent_code/{agent_name}/model.pt", "wb") as file:
            pickle.dump(model, file)

    # update anylsis file
    if os.path.isfile(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt"):
        with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "rb") as file:
            saved_analysis = pickle.load(file)

        saved_analysis["reward_history"].extend(analysis["reward_history"])
        saved_analysis["loss_history"].extend(analysis["loss_history"])
        saved_analysis["game_length"].extend(analysis["game_length"])
        saved_analysis["temp"].extend(analysis["temp"])
        saved_analysis["weight_history"].extend(analysis["weight_history"])
    else:
        saved_analysis = analysis


    with open(f"./agent_code/{agent_name}/analysis/analysis_{analysis_file_index}.pt", "wb") as file:
        pickle.dump(saved_analysis, file)


if __name__ == '__main__':
    main(sys.argv)
