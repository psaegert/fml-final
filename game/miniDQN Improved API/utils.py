import pickle
import os
import numpy as np
import random

def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if s_ != None:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if s_ != None:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if s_ != None:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if s_ != None:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if s_ != None:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if s_ != None:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_

def array_splits_of_size(N, array):
    return np.array_split(array, len(array) // N)

def state_to_features(game_state: dict, last_position) -> np.array:
    inputs = []

    # Get blocks around
    inputs += block_inputs(game_state)

    # Coin inputs
    inputs += coin_inputs(game_state)

    # Bomb inputs
    inputs += bomb_inputs(game_state)

    # Explosion inputs
    inputs += explosion_inputs(game_state)

    # Player inputs
    inputs += player_inputs(game_state)

    # Crate inputs
    inputs += crate_inputs(game_state)

    return np.array(inputs)

def block_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Get fields around and under player
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["field"][i][k] != 0 else 0)

    # Get bomb free field in each direction
    inputs.append(0)
    for i in range(x+1, min(16, x+5)):
        if game_state["field"][i][y] == 0: # Check if field is free
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break
        

    inputs.append(0)
    for i in range(x-1, max(-1, x-5), -1):
        if game_state["field"][i][y] == 0:
            if has_bomb(game_state, (i,y)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y+1] == 0 and has_bomb(game_state, (i,y+1)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][i][y-1] == 0 and has_bomb(game_state, (i,y-1)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y+1, min(16, y+5)):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    inputs.append(0)
    for i in range(y-1, -1, -1):
        if game_state["field"][x][i] == 0:
            if has_bomb(game_state, (x,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x+1][i] == 0 and has_bomb(game_state, (x+1,i)) == 0:
                inputs[-1] = 1
                break
            if game_state["field"][x-1][i] == 0 and has_bomb(game_state, (x-1,i)) == 0:
                inputs[-1] = 1
                break
        else:
            break

    # print(inputs[-4:])

    return inputs

def coin_inputs(game_state: dict):
    x, y = game_state["self"][3]
    
    # Find closest coin
    nearest_coin = None
    coin_distance = None
    for coin in game_state["coins"]:
        distance = np.sqrt((x - coin[0])**2 + (y - coin[1])**2)
        if coin_distance is None or distance < coin_distance:
            coin_distance = distance
            nearest_coin = coin

    if nearest_coin is None:
        return [0, 0]

    inputs = []
    inputs.append((nearest_coin[0] - x) / (coin_distance + 0.1))
    inputs.append((nearest_coin[1] - y) / (coin_distance + 0.1))

    return inputs

def bomb_inputs(game_state: dict):
    x, y = game_state["self"][3]
    inputs = []
    
    # Find relevant bombs
    fields = [(x, y), (x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    for field in fields:
        inputs.append(has_bomb(game_state, field))

    # Check if bomb is ready to place
    inputs.append(1 if game_state["self"][2] else 0)

    return inputs

def has_bomb(game_state, field):
    value = 0
    for bomb in game_state["bombs"]:
        coords = bomb[0]
        if field[0] - coords[0] != 0 and field[1] - coords[1] != 0:
            continue

        distance = np.sqrt((field[0] - coords[0])**2 + (field[1] - coords[1])**2)
        if distance > 3:
            continue

        value += 1 - distance * 0.25

    return value

def explosion_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]
    
    for i in [x-1, x, x+1]:
        for k in [y-1, y, y+1]:
            inputs.append(1 if game_state["explosion_map"][i][k] != 0 else 0)

    return inputs

def player_inputs(game_state: dict):
    x, y = game_state["self"][3]
    inputs = []

    # Find closest player
    closest_player = None
    player_distance = None
    for player in game_state["others"]:
        coords = player[3]
        distance = np.sqrt((x - coords[0])**2 + (y - coords[1])**2)
        if player_distance is None or distance < player_distance:
            player_distance = distance
            closest_player = coords

    if closest_player is None:
        inputs += [0, 0]
    else:
        inputs.append((closest_player[0] - x) / (player_distance + 0.1))
        inputs.append((closest_player[1] - y) / (player_distance + 0.1))

    # Check if player is in bomb range
    if closest_player and (closest_player[0] - x == 0 or closest_player[1] - y == 0) and player_distance < 4:
        inputs.append(1)
    else:
        inputs.append(0)
        

    return inputs

def crate_inputs(game_state: dict):
    inputs = []
    x, y = game_state["self"][3]

    # Find closest crate
    closest_crate = None
    crate_distance = None
    for i in range(len(game_state["field"])):
        for k in range(len(game_state["field"][i])):
            if game_state["field"][i][k] == 1:
                distance = np.sqrt((x - i)**2 + (y - k)**2)

                if crate_distance is None or distance < crate_distance:
                    crate_distance = distance
                    closest_crate = (i, k)

    # print(closest_crate)
    if closest_crate is None:
        inputs += [0, 0]
    else:
        if abs(closest_crate[0] - x) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[0] - x) / (crate_distance + 0.1))

        if abs(closest_crate[1] - y) <= 1:
            inputs.append(0)
        else:
            inputs.append((closest_crate[1] - y) / (crate_distance + 0.1))

        if closest_crate[0] - x == 1 and closest_crate[1] - y == 1:
            # print("case")
            inputs[-1], inputs[-2] = 0.5, 0.5

    # Check if player is next to crate
    fields = [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]
    inputs.append(0)
    for field in fields:
        if game_state["field"][field[0]][field[1]] == 1:
            inputs[-1] = 1
            break

    # print(inputs)
    return inputs


def temperature(T0, current_round, max_rounds, mean_round_length):
    return max(0.1, T0 * ((current_round - max_rounds) / max_rounds)**4), (mean_round_length if random.random() < 0.2 else 400)

def epsilon(current_round, max_rounds):
    return max(0.1, 0.5 - current_round/max_rounds)

class RewardModel:
    def __init__(self):
        self.actual_rewards = np.array([
            0, #"MOVED_LEFT"
            0, #"MOVED_RIGHT",
            0, #"MOVED_UP",
            0, #"MOVED_DOWN",
            0, #"WAITED",
            0, #"INVALID_ACTION",
            0, #"BOMB_DROPPED",
            0, #"BOMB_EXPLODED",
            0, #"CRATE_DESTROYED",
            0, #"COIN_FOUND",
            1, #"COIN_COLLECTED",
            5, #"KILLED_OPPONENT",
            0, #"KILLED_SELF",
            0, #"GOT_KILLED",
            0, #"OPPONENT_ELIMINATED",
            0, #"SURVIVED_ROUND"
            0, #MOVED_BACK
        ])
        self.rewards = np.array([
            -1, #MOVED_LEFT
            -1, #MOVED_RIGHT
            -1, #MOVED_UP
            -1, #MOVED_DOWN
            -3, #WAITED
            -5, #INVALID_ACTION
            -1, #BOMB_DROPPED
            0, #BOMB_EXPLODED
            30, #CRATE_DESTROYED
            0, #COIN_FOUND
            100, #COIN_COLLECTED
            200, #KILLED_OPPONENT
            0, #KILLED_SELF
            -300, #GOT_KILLED
            0, #OPPONENT_ELIMINATED
            0, #SURVIVED_ROUND
            0, #MOVED_BACK
        ]) / 10
        self.events = np.array([
            "MOVED_LEFT",
            "MOVED_RIGHT",
            "MOVED_UP",
            "MOVED_DOWN",
            "WAITED",
            "INVALID_ACTION",
            "BOMB_DROPPED",
            "BOMB_EXPLODED",
            "CRATE_DESTROYED",
            "COIN_FOUND",
            "COIN_COLLECTED",
            "KILLED_OPPONENT",
            "KILLED_SELF",
            "GOT_KILLED",
            "OPPONENT_ELIMINATED",
            "SURVIVED_ROUND",
            "MOVED_BACK"
        ])
        # self.create_data_file()

    def from_events(self, events):
        reward_sum = 0
        for e in events:
            reward_sum += self.rewards[np.where(self.events == e)[0][0]]
        return reward_sum
    
    def actual(self, events):
        reward_sum = 0
        for e in events:
            reward_sum += self.actual_rewards[np.where(self.events == e)[0][0]]
        return reward_sum
