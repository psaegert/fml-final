from tensorflow.keras.models import clone_model, Sequential, load_model
from tensorflow.keras.layers import Dense, Conv2D, MaxPool2D, Flatten, ReLU, Dropout, Input
from tensorflow.keras.optimizers import Adam
import tensorflow as tf
import pickle
import numpy as np
import os

tf.config.set_visible_devices([], 'GPU')

from utils import state_to_features, RewardModel, transform_state

class Agent:
    def __init__(self, filename):
        self.actions = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']
        try:
            self.model = load_model(filename)
            print("Loading saved model " + filename)
        except:
            self.model = Sequential([
                Input(shape=(36)),

                Dense(40),
                ReLU(),

                Dense(6, activation="linear")
            ])
            self.model.compile(loss="mse", optimizer=Adam(lr=0.0005), metrics=["accuracy"])
            print("Created new model")
        self.target = clone_model(self.model)
        self.target.set_weights(self.model.get_weights())
        self.update_target_every = 8
        self.target_model_update_counter = 0
        self.save_model_every = 1000
        self.save_model_counter = 0
        self.model.summary()
        self.discount = 0.5 # ->0.95
        self.analysis = {
            "loss": [],
            "accuracy": [],
            "actual_rewards": [],
            "aux_rewards": [],
            "T": [],
            "mean_round_length": [],
            "events": []
        }
        self.R = RewardModel()
    
    def learn(self, tsample):
        # for tr in [-1, 0, 1, 2, 3, 4, 5]:
        
        # initialize arrays
        S = np.empty((len(tsample), 36))
        A = np.empty(len(tsample), dtype=int)
        S_ = np.empty((len(tsample), 36))
        G = np.empty(len(tsample))

        # fill arrays and non-terminal state indices
        non_terminal_state_indices = []
        for i, t in enumerate(tsample):
            S[i, :] = t[0]
            A[i] = t[1]
            S_[i, :] = t[2]
            if type(t[2]) == np.ndarray: non_terminal_state_indices.append(i)
            G[i] = self.R.from_events(t[3])
        
        max_future_Qs = np.max(self.target.predict(S_[non_terminal_state_indices], batch_size=len(non_terminal_state_indices)), axis=1)
        
        # update returns based on max future Qs and discount
        G[non_terminal_state_indices] += max_future_Qs * self.discount
        
        # create response vector
        Qs = self.model.predict(S, batch_size=len(tsample))

        # update response vector
        for nt, a in zip(non_terminal_state_indices, A[non_terminal_state_indices]):
            Qs[nt, a] = G[nt]

        # fit on states and target response vector
        hist = self.model.fit(S, Qs, epochs=1, verbose=False, batch_size=32).history
        for k in ["loss", "accuracy"]: self.analysis[k].extend(hist[k])

        # update target model
        self.target_model_update_counter += 1
        if self.target_model_update_counter > self.update_target_every:
            self.target.set_weights(self.model.get_weights())
            self.target_model_update_counter = 0
        
        # save model
        self.save_model_counter += 1
        if self.save_model_counter > self.save_model_every:
            self.model.save("DQN")
            self.save_analysis()
            self.save_model_counter = 0

    def update_analysis(self, events_list, T, mean_round_length):
        self.analysis["actual_rewards"].extend([self.R.actual(events) for events in events_list])
        self.analysis["aux_rewards"].extend([self.R.from_events(events) for events in events_list])
        self.analysis["T"].append(T)
        self.analysis["mean_round_length"].append(mean_round_length)
        self.analysis["events"].extend([[np.where(self.R.events == e)[0][0] for e in events] for events in events_list])

    def save_analysis(self):
        if os.path.isfile("analysis.pt"):
            with open("analysis.pt", "rb") as file:
                analysis = pickle.load(file)
            
            for k in analysis.keys(): analysis[k].extend(self.analysis[k])

            with open("analysis.pt", "wb") as file:
                pickle.dump(analysis, file)
        
        else:
            with open("analysis.pt", "wb") as file:
                pickle.dump(self.analysis, file)
            
        self.analysis = {
            "loss": [],
            "accuracy": [],
            "actual_rewards": [],
            "aux_rewards": [],
            "T": [],
            "mean_round_length": [],
            "events": []
        }

    def act(self, state):
        p = self.model.predict(state_to_features(state))
        return self.actions[np.argmax(p)]

    