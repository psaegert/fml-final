import sys
import threading
from argparse import ArgumentParser
from time import sleep, time

import settings as s
from environment import BombeRLeWorld, GenericWorld
from fallbacks import pygame, tqdm, LOADED_PYGAME
from replay import ReplayWorld

from utils import *
from agent import Agent

import tensorflow as tf
import random
from collections import deque
import numpy as np

# Function to run the game logic in a separate thread
def game_logic(world: GenericWorld, user_inputs, args):
    last_update = time()
    while True:
        now = time()
        if args.turn_based and len(user_inputs) == 0:
            sleep(0.1)
            continue
        elif world.gui is not None and (now - last_update < args.update_interval):
            sleep(args.update_interval - (now - last_update))
            continue

        last_update = now
        if world.running:
            world.do_step(user_inputs.pop(0) if len(user_inputs) else 'WAIT')

A = Agent("DQN")
R = RewardModel()
T0 = 0.75
MAX_REPLAY_MEMORY_SIZE = 75_000
MIN_REPLAY_MEMORY_SIZE = 25_000 #200*32*4
replay_memory = deque(maxlen=MAX_REPLAY_MEMORY_SIZE)
trajectory_length = deque(maxlen=100)

def main(args):
    parser = ArgumentParser()

    subparsers = parser.add_subparsers(dest='command_name', required=True)

    # Run arguments
    play_parser = subparsers.add_parser("play")
    agent_group = play_parser.add_mutually_exclusive_group()
    agent_group.add_argument("--my-agent", type=str, help="Play agent of name ... against three rule_based_agents")
    agent_group.add_argument("--agents", type=str, nargs="+", default=["rule_based_agent"] * s.MAX_AGENTS, help="Explicitly set the agent names in the game")
    play_parser.add_argument("--train", default=0, type=int, choices=[0, 1, 2, 3, 4],
                             help="First … agents should be set to training mode")
    play_parser.add_argument("--continue-without-training", default=False, action="store_true")
    # play_parser.add_argument("--single-process", default=False, action="store_true")

    play_parser.add_argument("--n-rounds", type=int, default=10, help="How many rounds to play")
    play_parser.add_argument("--save-replay", default=False, action="store_true", help="Store the game as .pt for a replay")
    play_parser.add_argument("--no-gui", default=False, action="store_true", help="Deactivate the user interface and play as fast as possible.")

    # Replay arguments
    replay_parser = subparsers.add_parser("replay")
    replay_parser.add_argument("replay", help="File to load replay from")

    # Interaction
    for sub in [play_parser, replay_parser]:
        sub.add_argument("--fps", type=int, default=15, help="FPS of the GUI (does not change game)")
        sub.add_argument("--turn-based", default=False, action="store_true",
                         help="Wait for key press until next movement")
        sub.add_argument("--update-interval", type=float, default=0.1,
                         help="How often agents take steps (ignored without GUI)")

        # Video?
        sub.add_argument("--make-video", default=False, action="store_true",
                         help="Make a video from the game")

    args = parser.parse_args()
    if args.command_name == "replay":
        args.no_gui = False
        args.n_rounds = 1

    has_gui = not args.no_gui
    if has_gui:
        if not LOADED_PYGAME:
            raise ValueError("pygame could not loaded, cannot run with GUI")
        pygame.init()

    # Initialize environment and agents
    if args.command_name == "play":
        agents = []
        if args.train == 0 and not args.continue_without_training:
            args.continue_without_training = True
        if args.my_agent:
            agents.append((args.my_agent, len(agents) < args.train))
            args.agents = ["rule_based_agent"] * (s.MAX_AGENTS - 1)
        for agent_name in args.agents:
            agents.append((agent_name, len(agents) < args.train))

        world = BombeRLeWorld(args, agents)
    elif args.command_name == "replay":
        world = ReplayWorld(args)
    else:
        raise ValueError(f"Unknown command {args.command_name}")

    # Emulate Windows process spawning behaviour under Unix (for testing)
    # mp.set_start_method('spawn')

    user_inputs = []

    # Start game logic thread
    t = threading.Thread(target=game_logic, args=(world, user_inputs, args), name="Game Logic")
    t.daemon = True
    t.start()

    # Run one or more games
    train_round_index = 0
    round_index = 0
    total_steps = 0
    while train_round_index < args.n_rounds:
        start = time()
        
        if not world.running:
            world.ready_for_restart_flag.wait()
            world.ready_for_restart_flag.clear()

            mean_round_length = np.mean(trajectory_length) if len(trajectory_length) > 0 else 0

            new_events_list, new_replay_memory, T = world.new_round(
                # temperature(
                #     T0,
                #     train_round_index,
                #     args.n_rounds,
                #     mean_round_length
                # ),
                epsilon(
                    total_steps,
                    100_000
                ),
                A.model if round_index == 1 else None,
                A.model.get_weights()
            )
            
            A.discount = 0.5 + (0.95 - 0.5) * total_steps / 100_000

            additional_steps_to_learn_from = 0
            
            if round_index > 1:
                for trajectory in new_replay_memory:
                    trajectory_length.append(len(trajectory))
                    replay_memory.extend(trajectory)
                    additional_steps_to_learn_from += len(trajectory)
            
            if train_round_index > 0:
                A.update_analysis(new_events_list, T, mean_round_length)

        
        else:
            additional_steps_to_learn_from = 0


        # First render
        if has_gui:
            world.render()
            pygame.display.flip()

        round_finished = False
        last_frame = time()
        user_inputs.clear()

        # Main game loop
        while not round_finished:
            if has_gui:
                # Grab GUI events
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        if world.running:
                            world.end_round()
                        world.end()
                        return
                    elif event.type == pygame.KEYDOWN:
                        key_pressed = event.key
                        if key_pressed in (pygame.K_q, pygame.K_ESCAPE):
                            world.end_round()
                        if not world.running:
                            round_finished = True
                        # Convert keyboard input into actions
                        if s.INPUT_MAP.get(key_pressed):
                            if args.turn_based:
                                user_inputs.clear()
                            user_inputs.append(s.INPUT_MAP.get(key_pressed))

                # Render only once in a while
                if time() - last_frame >= 1 / args.fps:
                    world.render()
                    pygame.display.flip()
                    last_frame = time()
                else:
                    sleep_time = 1 / args.fps - (time() - last_frame)
                    if sleep_time > 0:
                        sleep(sleep_time)
            elif not world.running:
                round_finished = True
            else:
                # Non-gui mode, check for round end in 1ms
                sleep(0.001)
            
        # LEARN FROM TRANSITIONS
        if len(replay_memory) > MIN_REPLAY_MEMORY_SIZE:
            for _ in tqdm(range(additional_steps_to_learn_from)):
                A.learn(random.sample(replay_memory, 32))
            train_round_index += 1
            total_steps += additional_steps_to_learn_from
        
            print(f"Round {train_round_index}/{args.n_rounds} completed in {np.round(time() - start, 2)}s. Next checkpoint progress: {A.save_model_counter}/{A.save_model_every}")
        else:
            print(f"Filling replay memory {len(replay_memory)}/{MIN_REPLAY_MEMORY_SIZE}")
        
        round_index += 1

    world.end()

    # R.save_score(A.actual_rewards)


if __name__ == '__main__':
    main(sys.argv)
