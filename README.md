# Fundamentals of Machine Learning WS20/21: Final Project

Models marked with * are not mentioned in the Report.

## Data Analysis (Paul Saegert)

Jupyter notebooks in which general statistics about the game are analyzed.

## Game (Paul Saegert)

Includes all online or episodic Reinforcement Learning methods that are based on the imitator model architecture.

The reason why I duplicated the game files so often is that I wanted to apply self-learning to boost efficiency. The original API did not support a clean way to do this so I modified the API and improved the modifications with each new approach hence the different copies. Also, some of the models required different data structures which could not be implemented in the same game.

### Approximate Q Learning (AQL)

See section 4.1 (Abandoned Models: Approximate Q Learning)

### Deep Q Network (DQN)

See section 4.3 (Abandoned Models: Online Conv-DQN)

### Monte Carlo Policy Gradient (MCPG)*

Abandoned Model. Not noteworthy.

### miniDQN Improved API

Failed attempt of using oniumy's miniDQN architecture and training it using my improved and modified game API.

### Regression Forest (RF)*

Abandoned Model. Not noteworthy.

### Rule-Based Transition Miner

This copy of the game API was modified to collect and combine the transitions from all its agents and occasionally store them on a separate harddrive. Storage takes place in environment.py.

### Testing and Evaluation

Unchanged game API. This copy was only used to test and evaluate the trained models in the original game. The evaluation data of each agent is stored in its agent_code folder. The evaluation itself takes place in /agent_code/evaluationX.ipynb.

See subsections of section 3.1 (Results: imitator16 vs miniDQN vs ensemble vs rule_based_agent)

## Offline Learning (Paul Saegert)

Includes all offline Reinforcement Learning methods and other offline methods based on the imitator model architectore. The status of each experiment is indicated by WTT ('want to train'), F ('failed') or no prefix (useful results).

### Imitation

In here, different architectures have been trained to imitate the rule-based behaviour. Comparison is done in comparison.ipynb

See section 2.1 (Imitator)

### Reward Based

#### Deep Q Network (DQN)

See section 4.3 (Abandoned Models: Offline Conv-DQN)

#### Policy Gradient (PG)

See section 4.2 (Abandoned Models: Using the pre-trained model for Policy Gradient)

## RL_NN (oniumy / Dominik Langer)

Includes the final version of the miniDQN submodel used in the ensemble as well as important alternative models.

### miniDQN1

Final version of miniDQN with 36 inputs, 40 hidden neurons and 6 outputs. This submodel is already trained and is the one used in the ensemble. The agent code is stored in the folder rl_nn.

### miniDQN2

Improved version of the miniDQN with 37 inputs and 70 hidden neurons. This model is trained as well, but wasn't chosen as a submodel in our final model. The agent code is also stored in the folder rl_nn.

### miniDQN3

A third miniDQN version with a second hidden layer. Training failed to show any learning improvements, therefore the idea was retired. The agent code is also stored in the folder rl_nn.

### evolution_based

Early test to analyse an evolution based method. Development stopped after the miniDQN model was able to reliably collect all coins in training sessions with less than 200 played rounds, therefore the code does not yet implement a full evolution based approach. The agent code is stored in the folder my_agent.
