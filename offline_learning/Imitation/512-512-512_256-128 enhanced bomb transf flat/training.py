# To add a new cell, type ''
# To add a new markdown cell, type ' [markdown]'

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Dense, Conv2D, Flatten, Dropout, MaxPool2D, Activation, LeakyReLU, ReLU, Concatenate
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.optimizers import Adam

import kerastuner as kt

import os
import pickle
import numpy as np
from tqdm import tqdm

from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt

print(tf.__version__, tf.test.gpu_device_name())
# tf.config.set_visible_devices([], 'GPU')


DATA_DIRECTORY = 'Z:/fml-final/data/0-perfect-enhanced/'


def array_splits_of_size(N, array):
    return np.array_split(array, len(array) // N)

def joined_split_data_from_files(directory, filenames, random_state, test_size=0.2):
    X, e, y = list(), list(), list()

    for filename in filenames:
        with open(directory + filename, "rb") as file:
            paths = pickle.load(file)

            for path in paths:
                for transition in path:
                    X.append(transition[0][0])
                    e.append(transition[0][1])
                    y.append(transition[1])
    
    return (
        train_test_split(np.array(X), test_size=test_size, random_state=random_state),
        train_test_split(np.array(e), test_size=test_size, random_state=random_state),
        train_test_split(np.array(y), test_size=test_size, random_state=random_state)
    )

def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if s_ != None:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if s_ != None:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if s_ != None:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if s_ != None:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if s_ != None:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if s_ != None:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_


try:
    imitator = tf.keras.models.load_model("imitator")
except:
    game_input = Input(shape=(15, 15, 8))
    enhanced_input = Input(shape=(1,))

    x = Conv2D(512, (2, 2))(game_input)
    x = ReLU()(x)
    x = MaxPool2D(2, 2)(x)
    x = Dropout(0.5)(x)

    x = Conv2D(512, (2, 2))(x)
    x = ReLU()(x)
    x = MaxPool2D(2, 2)(x)
    x = Dropout(0.5)(x)

    x = Conv2D(512, (2, 2))(x)
    x = ReLU()(x)
    x = MaxPool2D(2, 2)(x)
    x = Dropout(0.5)(x)

    x = Flatten()(x)
    x_infused = Concatenate()([x, enhanced_input])

    x = Dense(256)(x_infused)
    x = ReLU()(x)
    x = Dropout(0.5)(x)

    x = Dense(128)(x)
    x = ReLU()(x)
    x = Dropout(0.2)(x)

    action_output = Dense(6, activation="softmax")(x)

    imitator = tf.keras.Model(inputs=[game_input, enhanced_input], outputs=action_output, name="enhanced_imitator")

    imitator.compile(loss="categorical_crossentropy", optimizer=Adam(lr=0.0001), metrics=["accuracy"])

imitator.summary()



loss_history = []
accuracy_history = []
val_loss_history = []
val_accuracy_history = []



file_batches = array_splits_of_size(1, os.listdir(DATA_DIRECTORY))
print(f"joined {len(os.listdir(DATA_DIRECTORY))} files to {len(file_batches)} batches.")

# used for train_test_split in each epoch because data is loaded and split multiple times but has to be split exactly the same way in order for the validation data to stay the same and not get mixed up with the training data
random_states = []

n_epochs = 3
for epoch in range(n_epochs):
    print(f"starting epoch {epoch+1}/{n_epochs} ...")
    
    for f, filenames in enumerate(file_batches):
        if epoch == 0: random_states.append(np.random.randint(2**31))

        for t in [-1, 0, 1, 2, 3, 4, 5]:

            (X_train, X_test), (e_train, e_test), (y_train, y_test) = joined_split_data_from_files(DATA_DIRECTORY, filenames, random_state=random_states[f])

            if t != -1:
                X_train_t, y_train_t = list(), list()
                X_test_t, y_test_t = list(), list()
                for i in range(len(X_train)):
                    st, at, _ = transform_state(t, X_train[i], y_train[i])
                    X_train_t.append(st)
                    y_train_t.append(at)
                    
                for i in range(len(X_test)):
                    st, at, _ = transform_state(t, X_test[i], y_test[i])
                    X_test_t.append(st)
                    y_test_t.append(at)
                
                X_train_t = np.array(X_train_t)
                X_test_t = np.array(X_test_t)
                y_train_t = np.array(y_train_t)
                y_test_t = np.array(y_test_t)

            else:
                X_train_t = X_train
                X_test_t = X_test
                y_train_t = y_train
                y_test_t = y_test
                
            hist = imitator.fit(x=[X_train_t, e_train], y=tf.one_hot(y_train_t, 6), epochs=1, batch_size=32, shuffle=True, verbose=True, validation_data=([X_test_t, e_test], tf.one_hot(y_test_t, 6))).history
            
            loss_history.extend(hist["loss"])
            accuracy_history.extend(hist["accuracy"])
            val_loss_history.extend(hist["val_loss"])
            val_accuracy_history.extend(hist["val_accuracy"])

        imitator.save("imitator")

        if os.path.isfile("training.pt"):
            with open("training.pt", "rb") as file:
                saved_analysis = pickle.load(file)
                saved_analysis["loss_history"].extend(loss_history)
                saved_analysis["accuracy_history"].extend(accuracy_history)
                saved_analysis["val_loss_history"].extend(val_loss_history)
                saved_analysis["val_accuracy_history"].extend(val_accuracy_history)

            with open("training.pt", "wb") as file:
                pickle.dump(saved_analysis, file)
                
        else:
            with open("training.pt", "wb") as file:
                pickle.dump({
                    "loss_history": loss_history,
                    "accuracy_history": accuracy_history,
                    "val_loss_history": val_loss_history,
                    "val_accuracy_history": val_accuracy_history
                }, file)