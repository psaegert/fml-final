# To add a new cell, type ''
# To add a new markdown cell, type ' [markdown]'

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Dense, Conv2D, Flatten, Dropout, MaxPool2D, Activation, ReLU
from tensorflow.keras.optimizers import Adam

import os
import pickle
import numpy as np
from tqdm import tqdm

from sklearn.model_selection import train_test_split

import matplotlib.pyplot as plt

print(tf.__version__, tf.test.gpu_device_name())
# tf.config.set_visible_devices([], 'GPU')

# with open("../expected_reward.pt", "rb") as file:
#     expected_reward = pickle.load(file)

DATA_DIRECTORY = 'Z:/fml-final/data/10-random-all-events/'

UPDATE_TARGET_EVERY = 5
target_update_counter = 0

DISCOUNT = 0.9

game_rewards = [
    0, #"MOVED_LEFT"
    0, #"MOVED_RIGHT",
    0, #"MOVED_UP",
    0, #"MOVED_DOWN",
    0, #"WAITED",
    0, #"INVALID_ACTION",

    0, #"BOMB_DROPPED",
    0, #"BOMB_EXPLODED",

    0, #"CRATE_DESTROYED",
    0, #"COIN_FOUND",
    1, #"COIN_COLLECTED",

    5, #"KILLED_OPPONENT",
    0, #"KILLED_SELF",

    0, #"GOT_KILLED",
    0, #"OPPONENT_ELIMINATED",
    0, #"SURVIVED_ROUND"
]

S_mean = np.array([0.21777778, 0.49713297, 0.28508925, 0.00787499, 0.00446413, 0.0041838 , 0.00444444, 0.00646703])
S_std = np.array([1, 2.62971455e-02, 1.07735946e-01, 7.84225165e-02, 4.40337037e-02, 4.39966307e-02, 1, 4.43786461e-02])

def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if type(s_) == np.ndarray:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if type(s_) == np.ndarray:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if type(s_) == np.ndarray:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if type(s_) == np.ndarray:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if type(s_) == np.ndarray:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if type(s_) == np.ndarray:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_

def array_splits_of_size(N, array):
    return np.array_split(array, len(array) // N)

def reward_from_events(events, died_at=-1):
    reward_sum = 0
    for event in events:
        reward_sum += game_rewards[event]

    # if died_at >= 0:
    #     reward_sum -= expected_reward[died_at]

    return reward_sum

def shuffled_transitions_from_file(directory, filenames):
    transitions = list()

    for filename in filenames:
        with open(directory + filename, "rb") as file:
            paths = pickle.load(file)

            for path in paths:
                transitions.extend(path)

    np.random.shuffle(transitions)
    return transitions

def save(analysis):
    if os.path.isfile("training.pt"):
        with open("training.pt", "rb") as file:
            saved_analysis = pickle.load(file)
            for k in saved_analysis.keys(): saved_analysis[k].extend(analysis[k])

        with open("training.pt", "wb") as file:
            pickle.dump(saved_analysis, file)
            
    else:
        with open("training.pt", "wb") as file:
            pickle.dump(analysis, file)

    return {
        "loss": [],
        "accuracy": [],
    }

def normalize(S):
    return (S - S_mean) / S_std

try:
    DQN = tf.keras.models.load_model("DQN")
    print("INFO Loaded saved model")
except:
    print("INFO Creating new model")
    DQN = Sequential([
        Conv2D(512, (2, 2), input_shape=(15, 15, 8)),
        ReLU(),
        MaxPool2D(2, 2),
        # Dropout(0.5),

        Conv2D(512, (2, 2)),
        ReLU(),
        MaxPool2D(2, 2),
        # Dropout(0.5),

        Conv2D(512, (2, 2)),
        ReLU(),
        MaxPool2D(2, 2),
        # Dropout(0.5),

        Flatten(),

        Dense(256),
        ReLU(),
        # Dropout(0.5),

        Dense(128),
        ReLU(),
        # Dropout(0.2),

        Dense(6, activation="linear")
    ])

    DQN.compile(loss="mse", optimizer=Adam(lr=0.005), metrics=["accuracy"])

target = tf.keras.models.clone_model(DQN)
target.set_weights(DQN.get_weights())

DQN.summary()


analysis = {
    "loss": [],
    "accuracy": [],
}


file_batches = array_splits_of_size(1, os.listdir(DATA_DIRECTORY))
print(f"joined {len(os.listdir(DATA_DIRECTORY))} files to {len(file_batches)} batches.")

for f, filenames in tqdm(enumerate(file_batches)):
    transitions = shuffled_transitions_from_file(DATA_DIRECTORY, filenames)

    for training_batch in array_splits_of_size(32, transitions):

        S = np.empty((len(training_batch), 15, 15, 8))
        A = np.empty(len(training_batch), dtype=int)
        S_ = np.empty((len(training_batch), 15, 15, 8))
        G = np.empty(len(training_batch))

        for transformation in [-1, 0, 1, 2, 3, 4, 5]:

            for i, t in enumerate(training_batch):
                S[i, :, :, :] = t[0]
                A[i] = t[1]
                S_[i, :, :, :] = t[2]
                G[i] = reward_from_events(t[3])
            
            if transformation != -1:
                for i, (s, a, s_) in enumerate(zip(S, A, S_)): S[i], A[i], S_[i] = transform_state(transformation, s, a, s_)

            non_terminal_state_indices = []
            for i, s_ in enumerate(S_):
                if type(s_) == np.ndarray: non_terminal_state_indices.append(i)
            
            max_future_Qs = np.max(target.predict(S_[non_terminal_state_indices], batch_size=len(non_terminal_state_indices)), axis=1)
            
            G[non_terminal_state_indices] += max_future_Qs * DISCOUNT
            
            Qs = DQN.predict(normalize(S), batch_size=len(training_batch))
            # print(Qs[0])

            for nt, a in zip(non_terminal_state_indices, A[non_terminal_state_indices]):
                Qs[nt, a] = G[nt]

            # print(Qs[0])
            # print()

            hist = DQN.fit(normalize(S), Qs, epochs=1, verbose=False, batch_size=len(training_batch)).history
            for k in analysis.keys(): analysis[k].extend(hist[k])

            target_update_counter += 1
            if target_update_counter > UPDATE_TARGET_EVERY:
                target.set_weights(DQN.get_weights())
                target_update_counter = 0

    DQN.save("DQN")
    analysis = save(analysis)