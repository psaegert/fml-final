# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import numpy as np
import pickle

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPool2D, Dropout, Flatten, Dense, ReLU
from tensorflow.keras.optimizers import Adam

import tensorflow_probability as tfp

from tqdm import tqdm

import sys
import os

tf.config.set_visible_devices([], 'GPU')


# %%
DATA_DIRECTORY = 'Z:/fml-final/data/10-random-all-events/'

DISCOUNT = 0.9


game_rewards = [
    0, #"MOVED_LEFT"
    0, #"MOVED_RIGHT",
    0, #"MOVED_UP",
    0, #"MOVED_DOWN",
    -0.2, #"WAITED",
    -0.5, #"INVALID_ACTION",

    0.1, #"BOMB_DROPPED",
    0, #"BOMB_EXPLODED",

    0.1, #"CRATE_DESTROYED",
    0.1, #"COIN_FOUND",
    1, #"COIN_COLLECTED",

    5, #"KILLED_OPPONENT",
    0, #"KILLED_SELF",

    0, #"GOT_KILLED",
    0, #"OPPONENT_ELIMINATED",
    0, #"SURVIVED_ROUND"
]

# %%
with open("../../../data analysis/expected_reward.pt", "rb") as file:
    expected_reward = pickle.load(file)


# %%
def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if s_ != None:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if s_ != None:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if s_ != None:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if s_ != None:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if s_ != None:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if s_ != None:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_

def reward_from_events(events, died_at=-1):
    reward_sum = 0
    for event in events:
        reward_sum += game_rewards[event]
    return reward_sum

# %%
class PGModel:
    def __init__(self, path=None, lr=0.0001, DISCOUNT=0.9): #lr
        self.DISCOUNT = DISCOUNT
        self.lr = lr

        if "imitator" in path:
            saved_model = tf.keras.models.load_model(path)

            # remove dropout layers 3, 7, 11, 15, 18 by reconnecting model
            x = saved_model.layers[1](saved_model.layers[0].output)
            for i, layer in zip([2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 16, 17, 19], saved_model.layers):
                x = saved_model.layers[i](x)
            self.policy = tf.keras.Model(saved_model.input, x, name="MCPG")

            self.policy.compile(loss="categorical_crossentropy", optimizer=Adam(lr=lr), metrics=["accuracy"])
            self.policy.summary()

            del saved_model
        
        else:
            self.policy = tf.keras.models.load_model(path)

    def update(self, feature_memory, action_memory, reward_memory):
        rewards = np.flip(np.array(reward_memory, dtype=np.float32))

        sum_reward = 0
        G = []
        for r in rewards:
            sum_reward = r + self.DISCOUNT*sum_reward
            G.append(sum_reward)
        G.reverse()
                
        losses = []
        confidence = []

        for step, (s, a, g) in enumerate(zip(feature_memory, action_memory, G)):
            if g == 0: continue
            
            for transformation in [-1, 0, 1, 2, 3, 4, 5]:

                if transformation != -1: s, a, _ = transform_state(transformation, s, a)

                with tf.GradientTape() as tape:
                    # print(g)

                    s_tensor = tf.convert_to_tensor(s.reshape(1, 15, 15, 8).astype(np.float32), dtype=tf.float32)
                    
                    # prevent infinities after log and zero gradient
                    p = self.policy(s_tensor) * (1 - 2*sys.float_info.epsilon) + sys.float_info.epsilon
                    confidence.append(np.max(p))
                    
                    dist = tfp.distributions.Categorical(probs=p)
                    log_prob = dist.log_prob(a)

                    loss = -tf.squeeze(log_prob) * (g - expected_reward[step])

                    gradient = tape.gradient(loss, self.policy.trainable_variables)
                    self.policy.optimizer.apply_gradients(zip(gradient, self.policy.trainable_variables))
                
                losses.append(loss.numpy())

        return losses, confidence


# %%
try:
    model = PGModel("./MCPG")
    print("Loaded checkpoint")
except:
    model = PGModel("../../1_imitation/512-512-512_256-128/imitator")
    print("Loaded imitator")



# %%
analysis = {"loss_history": [], "confidence": []}


# %%
for filename in tqdm(os.listdir(DATA_DIRECTORY)[2:]):
    with open(DATA_DIRECTORY + filename, "rb") as file:
        replay_paths = pickle.load(file)

        for replay_path in tqdm(replay_paths):
            losses, confidence = model.update(
                [transition[0] for transition in replay_path],
                [transition[1] for transition in replay_path],
                [reward_from_events(transition[3]) for transition in replay_path],
            )
            analysis["loss_history"].append(losses)
            analysis["confidence"].append(confidence)

    # save checkpoints
    model.policy.save("MCPG")


    if os.path.isfile("training.pt"):
        with open("training.pt", "rb") as file:
            saved_analysis = pickle.load(file)
            for k in saved_analysis.keys(): saved_analysis[k].extend(analysis[k])
    else:
        saved_analysis = analysis

    with open("training.pt", "wb") as file:
        pickle.dump(saved_analysis, file)