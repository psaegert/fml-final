# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import numpy as np
import pickle

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.layers import Conv2D, MaxPool2D, Dropout, Flatten, Dense, ReLU
from tensorflow.keras.optimizers import Adam


import tensorflow_probability as tfp

from tqdm import tqdm
import os

import sys

tf.config.set_visible_devices([], 'GPU')

game_rewards = [
    0, #"MOVED_LEFT"
    0, #"MOVED_RIGHT",
    0, #"MOVED_UP",
    0, #"MOVED_DOWN",
    0, #"WAITED",
    0, #"INVALID_ACTION",

    0, #"BOMB_DROPPED",
    0, #"BOMB_EXPLODED",

    0, #"CRATE_DESTROYED",
    0, #"COIN_FOUND",
    1, #"COIN_COLLECTED",

    5, #"KILLED_OPPONENT",
    0, #"KILLED_SELF",

    0, #"GOT_KILLED",
    0, #"OPPONENT_ELIMINATED",
    0, #"SURVIVED_ROUND"
]

# %%
DATA_DIRECTORY = 'Z:/fml-final/data/10-random-all-events/'


# %%
# with open("../../expected_reward.pt", "rb") as file:
#     expected_reward = pickle.load(file)

# %%
def transform_state(t, s, a, s_=None):
    # ACTIONS = ['UP', 'RIGHT', 'DOWN', 'LEFT', 'WAIT', 'BOMB']

    # mirror x -> -x
    if t == 0:
        s = np.flip(s, axis=1)
        if s_ != None:
            s_ = np.flip(s_, axis=1)
        # switch left, right
        a = [0, 3, 2, 1, 4, 5][a]
    # mirror y -> -y
    elif t == 1:
        s = np.flip(s, axis=0)
        if s_ != None:
            s_ = np.flip(s_, axis=0)
        # switch up, down
        a = [2, 1, 0, 3, 4, 5][a]
    # mirror point
    elif t == 2:
        s = np.flip(np.flip(s, axis=0), axis=1)
        if s_ != None:
            s_ = np.flip(np.flip(s_, axis=0), axis=1)
        # switch up, down and left, right
        a = [2, 3, 0, 1, 4, 5][a]
    # transpose
    elif t == 3:
        s = np.transpose(s, axes=(1, 0, 2))
        if s_ != None:
            s_ = np.transpose(s_, axes=(1, 0, 2))
        # switch up, left and down, right
        a = [3, 2, 1, 0, 4, 5][a]
    # rotate left
    elif t == 4:
        s = np.rot90(s, 1)
        if s_ != None:
            s_ = np.rot90(s, 1)
        # rotate left
        a = [3, 0, 1, 2, 4, 5][a]
    # rotate right
    elif t == 5:
        s = np.rot90(s, -1)
        if s_ != None:
            s_ = np.rot90(s, -1)
        # rotate right
        a = [1, 2, 3, 0, 4, 5][a]
    
    return s, a, s_

def reward_from_events(events, died_at=-1):
    reward_sum = 0
    for event in events:
        reward_sum += game_rewards[event]

    # if died_at >= 0:
    #     reward_sum -= expected_reward[died_at]

    return reward_sum


# %%
class PGModel:
    def __init__(self, path=None, lr=0.001, DISCOUNT=0.9):
        self.DISCOUNT = DISCOUNT
        self.lr = lr

        try:
            self.policy = tf.keras.models.load_model(path)
            print("Loaded checkpoint")
        except:
            print("Created new model")
            self.policy = tf.keras.models.Sequential([
                Conv2D(512, (2, 2), input_shape=(15, 15, 8)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5),

                Conv2D(512, (2, 2)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5),

                Conv2D(512, (2, 2)),
                ReLU(),
                MaxPool2D(2, 2),
                # Dropout(0.5),

                Flatten(),

                Dense(256),
                ReLU(),
                # Dropout(0.5),

                Dense(128),
                ReLU(),
                # Dropout(0.2),

                Dense(6, activation="softmax")
            ])

            self.policy.compile(loss="categorical_crossentropy", optimizer=Adam(lr=lr), metrics=["accuracy"])

    def update(self, feature_memory, action_memory, reward_memory):
        rewards = np.flip(np.array(reward_memory, dtype=np.float32))

        # died_in_step = len(reward_memory)
        # rewards[-1] -= expected_reward[died_in_step]

        sum_reward = 0
        G = []
        for r in rewards:
            sum_reward = r + self.DISCOUNT*sum_reward
            G.append(sum_reward)
        G.reverse()
        
        losses = []
        confidence = []
        
        for s, a, g in zip(feature_memory, action_memory, G):
            if g == 0: continue

            for transformation in [-1, 0, 1, 2, 3, 4, 5]:

                if transformation != -1: s, a, _ = transform_state(transformation, s, a)

                with tf.GradientTape() as tape:
                    s_tensor = tf.convert_to_tensor(s.reshape(1, 15, 15, 8).astype(np.float32), dtype=tf.float32)
                    
                    # prevent infinities after log and zero gradient
                    p = self.policy(s_tensor, training=True) * (1 - sys.float_info.epsilon) + sys.float_info.epsilon

                    # print("prob", end=" ")
                    # for probs in p.numpy()[0]: print(probs, end="\t")
                    # print()

                    dist = tfp.distributions.Categorical(probs=p)
                    log_prob = dist.log_prob(a)

                    loss = -log_prob * g
                    # print("loss", loss.numpy()[0], "\n")
                    gradient = tape.gradient(loss, self.policy.trainable_variables)
                    self.policy.optimizer.apply_gradients(zip(gradient, self.policy.trainable_variables))
                
                confidence.append(np.max(p))
                losses.append(loss.numpy())

        return losses, confidence

model = PGModel("MCPG")

# %%
analysis = {"loss_history": [], "confidence": []}


# %%
for filename in tqdm(os.listdir(DATA_DIRECTORY)):
    with open(DATA_DIRECTORY + filename, "rb") as file:
        replay_paths = pickle.load(file)

        for replay_path in tqdm(replay_paths):
            losses, confidence = model.update(
                [transition[0] for transition in replay_path],
                [transition[1] for transition in replay_path],
                [reward_from_events(transition[2]) for transition in replay_path],
            )
            analysis["loss_history"].append(losses)
            analysis["confidence"].append(confidence)

    # save checkpoints
    model.policy.save("MCPG")


    if os.path.isfile("training.pt"):
        with open("training.pt", "rb") as file:
            saved_analysis = pickle.load(file)
            for k in saved_analysis.keys(): saved_analysis[k].extend(analysis[k])
    else:
        saved_analysis = analysis

    with open("training.pt", "wb") as file:
        pickle.dump(saved_analysis, file)